ENV["VERSION"] or abort "VERSION= must be specified"
manifest = File.readlines('.manifest').map! { |x| x.chomp! }
require 'wrongdoc'
extend Wrongdoc::Gemspec
name, summary, title = readme_metadata

Gem::Specification.new do |s|
  s.name = %q{kgio-monkey}
  s.version = ENV["VERSION"].dup
  s.homepage = Wrongdoc.config[:rdoc_url]
  s.authors = ["kgio hackers"]
  s.description = readme_description
  s.email = %q{kgio@bogomips.org}
  s.extra_rdoc_files = extra_rdoc_files(manifest)
  s.files = manifest
  s.rdoc_options = rdoc_options
  s.executables = %w(flipper)
  s.summary = summary
  s.test_files = Dir['test/test_*.rb']
  s.extensions = %w(ext/kgio_monkey/extconf.rb)

  s.add_dependency('kgio', '~> 2.6')
  s.add_development_dependency('olddoc', '~> 1.0')
  s.add_development_dependency('tdb')
  s.add_development_dependency('dalli')
  s.add_development_dependency('memcached')
  # s.license = %w(LGPL) # disabled for compatibility with older RubyGems
end
