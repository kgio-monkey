# client mode:
# The following maps a plain client port (9999) to an IMAPS (port 993)
ssl_client "127.0.0.1:9999" => "imap.example.com:993" do
  # client_certificate MUST be specified for client mode
  client_certificate "~/.mutt/imap.example.com.pem"
end

# server mode:
# The following maps IMAPS (port 993) to plain IMAP (port 143)
ssl_server 993 => "127.0.0.1:143" do
  certificate "/etc/ssl/server.crt"
  certificate_key "/etc/ssl/server.key"
end
