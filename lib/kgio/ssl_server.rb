# extend an existing Kgio::TCPServer with this module to have it return
# Kgio:SSL objects instead of Kgio::Socket IO objects
module Kgio::SSLServer
  # :stopdoc:
  attr_accessor :ssl_ctx
  attr_accessor :kgio_ssl_class

  ACCEPT_FLAGS = Kgio::SOCK_NONBLOCK | Kgio::SOCK_CLOEXEC

  def self.extended(obj)
    obj.kgio_ssl_class ||= Kgio::SSL
  end

  def kgio_accept(io_klass = nil, flags = 0)
    io = super(io_klass, ACCEPT_FLAGS) and
      return @kgio_ssl_class.new(io, @ssl_ctx)
  end

  def kgio_tryaccept(io_klass = nil, flags = 0)
    io = super(io_klass, ACCEPT_FLAGS) and
      return @kgio_ssl_class.new(io, @ssl_ctx)
  end
  # :startdoc
end
