# -*- encoding: binary -*-
require "kgio"
require "openssl"
require "kgio_monkey_ext"
require "kgio/ssl"

module Kgio
  autoload :SSLServer, "kgio/ssl_server"
end
