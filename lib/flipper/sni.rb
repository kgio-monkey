class Flipper::SNI
  BAD_HOSTNAME = lambda do |h|
    raise ArgumentError, "bad hostname: #{h.inspect}"
  end

  def initialize(default_action = BAD_HOSTNAME)
    @aref = {}
    @find = {}
    @default_action = default_action
  end

  def lookup(hostname)
    @aref[hostname] ||
      @find.each { |re,ctx| re === hostname and return ctx } ||
      ( @default_action.respond_to?(:call) and @default_action.call(hostname) )
  end

  def register(hostname, ctx)
    case hostname
    when String
      @aref[hostname.downcase] = ctx
    else
      @find[hostname] = ctx
    end
  end

  alias clear initialize

  def servername_cb
    lambda { |(_,hostname)| hostname.downcase!; lookup(hostname) }
  end
end
