# -*- encoding: binary -*-
class Flipper::Socket < Kgio::Socket
  include Flipper::Base
  def self.start
    super(@connect_addr)
  end

  alias kgio_tryclose close
end
