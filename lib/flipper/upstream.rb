# -*- encoding: binary -*-
# extends /classes/ to be used for upstreams
module Flipper::Upstream
  attr_reader :upstream_addr

  def upstream_setup(connect_addr)
    @connect_addr = connect_addr
    host, port = Socket.unpack_sockaddr_in(connect_addr).reverse!
    host = "[#{host}]" if host =~ /:/
    @upstream_addr = "#{host}:#{port}"
  end
end
