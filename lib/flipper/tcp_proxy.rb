# -*- encoding: binary -*-

# this class is only for TCP <-> TCP connections
class Flipper::TCPProxy
  attr_reader :to_io

  ACCEPT_FLAGS = Kgio::SOCK_NONBLOCK | Kgio::SOCK_CLOEXEC

  def initialize(ssl_ctx, listener, connect_addr, tlsext_hostname)
    @ssl_ctx = Kgio::Monkey!(ssl_ctx)
    listener.setsockopt(Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1)
    listener.setsockopt(Socket::SOL_SOCKET, Socket::SO_KEEPALIVE, 1)
    @to_io = listener
    if tlsext_hostname
      @upstream_class = Class.new(Flipper::SSLUpstream)
      @upstream_class.ssl_ctx = @ssl_ctx
      @upstream_class.ssl_session = nil
      @upstream_class.tlsext_hostname = tlsext_hostname
    else # SSL server
      @upstream_class = Class.new(Flipper::Socket)
      @to_io.extend(Kgio::SSLServer)
      @to_io.ssl_ctx = @ssl_ctx
      @to_io.kgio_ssl_class = Flipper::SSLAccepted
    end
    @upstream_class.extend(Flipper::Upstream)
    @upstream_class.upstream_setup(connect_addr)
  end

  # yields the newly connected client, which will have a tied upstream already.
  def accept_start
    while client = @to_io.kgio_tryaccept(Flipper::Socket, ACCEPT_FLAGS)
      upstream = @upstream_class.start
      io = upstream.to_io
      io.setsockopt(Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1)
      io.setsockopt(Socket::SOL_SOCKET, Socket::SO_KEEPALIVE, 1)
      client.tie!(upstream)
      yield(upstream.tie!(client))
    end
  end
end
BasicSocket.do_not_reverse_lookup = true
require "flipper/upstream"
