# -*- encoding: binary -*-
module Flipper::MemcacheCommon
  KEYPACK = "m0"
  def key(session_id)
    [ session_id ].pack(KEYPACK)
  end

  def session_new_cb
    lambda do |(_,sess)|
      der = sess.to_der
      der.size <= 4096 and @cache.set(key(sess.id), der)
    end
  end

  def session_get_cb
    lambda do |(_,sess_id)|
      der = @cache.get(key(sess_id)) and return OpenSSL::SSL::Session.new(der)
    end
  end

  def session_remove_cb
    lambda do |sess|
      @cache.delete(key(sess.id))
    end
  end
end
