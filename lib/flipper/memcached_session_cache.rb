# -*- encoding: binary -*-
require "memcached"

# The memcached session cache for Flipper uses a default :timeout
# of 0.005 seconds.  This means your worst-case SSL accept rate will
# be ~200 connections/second if your connection to memcached is
# slow or non-responsive.  Providing :timeout yourself will
# override this.
class Flipper::MemcachedSessionCache
  # :stopdoc:
  include Flipper::MemcacheCommon

  # kinder, gentler memcached client that doesn't raise Memcached::NotFound
  class KGMemcached < Memcached
    def get(*args)
      super
    rescue Memcached::NotFound
    end

    def delete(*args)
      super
      true
    rescue Memcached::NotFound
    end
  end

  def initialize(servers, opts = {})
    opts[:timeout] ||= 0.005
    @cache = KGMemcached.new(servers, opts)
  end
  # :startdoc:
end
