# -*- encoding: binary -*-
class Flipper::Configurator
  def initialize
    @services = {} # acceptor => connector
    @context_opts = {}
    @ctx = nil
    @logger = nil
  end

  def ssl_server(mapping, &block)
    map(mapping, &block)
  end

  def ssl_client(mapping, &block)
    map(mapping, &block)[-1][:ssl_client] = true
  end

  def map(mapping, &block)
    mapping.size == 1 or raise ArgumentError, "usage: map(accept => connect)"
    accept, connect = mapping.first
    accept, connect = expand_addr(accept), expand_addr(connect)
    @ctx = {}
    instance_eval(&block)
    @services[accept] = [ connect, @ctx ]
    ensure
      @ctx = nil
  end

  def logger(logger = nil)
    logger ? @logger = logger : @logger
  end

  def run # :nodoc:
    poller = Flipper::Poller.new(@logger)
    @services.each { |mapping| poller.add_proxy(to_proxy(mapping.flatten!)) }
    yield if block_given? # intended to drop perms/chdir/setsid
    poller.run
  end

  def protocols(protocols)
    @ctx[:ssl_protocols] = protocols
  end

  def ssl_version(version)
    @ctx[:ssl_version] = version
  end

  def certificate(path)
    @ctx[:ssl_certificate] = path
  end

  def certificate_key(path)
    @ctx[:ssl_certificate_key] = path
  end

  def ca_path(path)
    @ctx[:ssl_ca_path] = path
  end

  def verify_depth(depth)
    @ctx[:ssl_verify_depth] = depth
  end

  def verify_mode(mode)
    @ctx[:ssl_verify_mode] = mode
  end

  def client_certificate(path)
    @ctx[:ssl_client_certificate] = path
  end

  def client(bool)
    @ctx[:ssl_client] = bool
  end

  # expands pathnames of sockets if relative to "~" or "~username"
  # expands "*:port and ":port" to "0.0.0.0:port"
  def expand_addr(address) #:nodoc:
    return "0.0.0.0:#{address}" if Integer === address

    case address
    when %r{\A(?:\*:)?(\d+)\z}
      "0.0.0.0:#$1"
    when %r{\A\[([a-fA-F0-9:]+)\]:(\d+)\z}, %r{\A(.*):(\d+)\z}
      addr, port = $1, $2.to_i
      packed = Socket.pack_sockaddr_in(port, addr)
      port, ip = Socket.unpack_sockaddr_in(packed)
      /:/ =~ ip ? "[#{addr}]:#{port}" : "#{addr}:#{port}"
    else
      raise ArgumentError, "address=#{address} not understood"
    end
  end

  def parse_addr(address) # :nodoc:
    address =~ %r{\A\[?(.+)?\]?:(\d+)\z} or
        raise ArgumentError, "couldn't parse #{address.inspect}"
    [ $1, $2.to_i ]
  end

  def to_proxy(mapping)
    accept, connect, ctx = *mapping
    client = ctx[:ssl_client]
    host, port = parse_addr(accept)
    listener = Kgio::TCPServer.new(host, port)
    host, port = parse_addr(connect)
    connect = Socket.pack_sockaddr_in(port, host)
    ctx = Flipper.ssl_context(ctx)
    Flipper::TCPProxy.new(ctx, listener, connect, client ? host : nil)
  end
end
