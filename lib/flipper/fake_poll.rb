# -*- encoding: binary -*-
# :enddoc:
# Kgio.poll isn't possible to support under Ruby 1.8 (MRI) because of
# its green-threading model already based on select().  So we emulate
# the subset of its functionality that we need for Flipper::Poller
module Flipper
  FakePoll = lambda do |pollset|
    tmp = { :wait_readable => [], :wait_writable => [] }
    pollset.each { |io,val| tmp[val] << io }
    if rv = IO.select(tmp[:wait_readable], tmp[:wait_writable])
      pollset.clear
      rv.flatten!.each { |io| pollset[io] = :ignored }
      return pollset
    end
  end
end
