# -*- encoding: binary -*-
require "dalli"

# The dalli session cache for Flipper uses a default :socket_timeout
# of 0.005 seconds.  This means your worst-case SSL accept rate will
# be ~200 connections/second if your connection to memcached is
# slow or non-responsive.  Providing :socket_timeout yourself will
# override this.
class Flipper::DalliSessionCache
  # :stopdoc:
  include Flipper::MemcacheCommon
  def initialize(servers, opts = {})
    opts[:socket_timeout] ||= 0.005
    @cache = Dalli::Client.new(servers, opts)
  end
  # :startdoc:
end
