# -*- encoding: binary -*-
require "tdb"

class Flipper::TDBSessionCache
  include TDB::HashFunctions

  def initialize(path, size, opts = {})
    @size = size
    opts[:tdb_flags] ||= TDB::NOSYNC | TDB::VOLATILE
    opts[:hash] ||= :murmur2
    if path
      @tdb = TDB.new(path, opts)
    else
      require "tempfile"
      tmp = Tempfile.new("tdb_session_cache")
      @tdb = TDB.new(tmp.path, opts)
      tmp.close!
    end
  end

  def key(session_id)
    (tdb_hash_murmur2(session_id) % @size).to_s(16)
  end

  def session_new_cb
    lambda do |(_,sess)|
      der = sess.to_der
      der.size <= 4096 and @tdb[key(sess.id)] = der
    end
  end

  def session_get_cb
    lambda do |(_,sess_id)|
      if der = @tdb[key(sess_id)]
        session = OpenSSL::SSL::Session.new(der)
        session.id == sess_id ? session : nil
      end
    end
  end

  def session_remove_cb
    lambda do |sess|
      @tdb.nuke!(key(sess.id))
    end
  end
end
