# -*- encoding: binary -*-

# in-memory buffering should always be safe since Flipper shouldn't
# try to read unless it knows the pair's write buffers are cleared.
module Flipper::Base
  attr_accessor :tied, :state

  def tie!(tied)
    @wbuf = ""
    @state = nil
    @tied = tied
  end

  def log_save_session(logger)
    reused = session_reused?
    if Flipper::SSLUpstream === self
      u = self.class
      c = tied
      u.ssl_session = session unless reused
    else
      u = tied.class
      c = to_io
    end
    logger.info("#{c.kgio_addr} -> #{u.upstream_addr} " \
             "#{compression? ? 1 : 0}/#{expansion? ? 1 : 0} #{reused ? 1 : 0}")
  end

  def update_state(logger)
    return if @state
    log_save_session(logger) if Kgio::SSL === self
    @state = :active
  end

  # returns nil if buffer is empty
  # returns :wait_readable or :wait_readable if blocked
  def buffer_flush
    return if 0 == @wbuf.size
    case rc = kgio_trywrite(@wbuf)
    when nil
      return @wbuf = ""
    when String
      @wbuf = rc
    else
      return rc
    end while true
  end

  def trywrite(buf, logger)
    rc = kgio_trywrite(buf)
    Symbol === rc ? @wbuf << buf : update_state(logger)
    rc
  end
end
