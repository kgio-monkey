# -*- encoding: binary -*-
require "logger"

class Flipper::Poller
  attr_reader :proxies

  POLL = Kgio.respond_to?(:poll) ? Kgio.method(:poll) : Flipper::FakePoll

  def initialize(logger = Logger.new($stderr))
    @rbuf = ""
    @pollset = {}
    @proxies = {}
    @to_drop = {}
    @logger = logger
    logger.info "LOG FORMAT: client -> upstream compression/expansion reused?"
  end

  def add_proxy(proxy)
    @proxies[proxy] = :wait_readable
  end

  def run
    begin
      POLL.call(@pollset.merge(@proxies)).each do |io, _|
        io.respond_to?(:accept_start) ? run_accept(io) : run_connected(io)
      end
      expire!
    rescue => err
      log_error(err)
    end while true
  end

  def expire!
    now = Time.now
    @to_drop.each { |io, expire_at| now > expire_at and drop!(io) }
  end

  def log_error(err)
    @logger.error("#{err.message} (#{err.class})")
    err.backtrace.each { |line| @logger.error(line) }
  end

  def run_accept(io)
    io.accept_start { |c| @pollset[c] = @pollset[c.tied] = :wait_writable }
  end

  def run_close(io)
    io.closed? and return io.tied.closed? ? nil : run_close(io.tied)
    if Symbol === (rc = io.kgio_tryclose)
      io.state = :closing
      @to_drop[io] ||= Time.now + 30
      return @pollset[io] = rc
    end
    # success
    @pollset.delete(io)
    run_close(io.tied)
  end

  def run_connected(io)
    return @pollset.delete(io) if io.closed?
    :closing == io.state and return run_close(io)
    Symbol === (rc = io.buffer_flush) and return @pollset[io] = rc

    case rc = io.kgio_tryread(0x4000, @rbuf)
    when Symbol
      @pollset[io] = rc
      return @pollset[io.tied] ||= :wait_readable
    when String
      io.update_state(@logger)
      case rc = io.tied.trywrite(rc, @logger)
      when Symbol
        @pollset.delete(io)
        return @pollset[io.tied] = rc # data got buffered
      when String # retry with new rc
      when nil
        break # use outer loop to read more from original io
      end while true
    when nil # EOF
      return run_close(io)
    end while true
    rescue => err
      log_error(err)
      drop!(io)
  end

  def drop!(io)
    @pollset.delete(io)
    @pollset.delete(io.tied)
    [ io.to_io, io.tied.to_io ].each { |x| x.closed? or x.close }
  end
end
