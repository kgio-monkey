# -*- encoding: binary -*-
class Flipper::SSLUpstream < Kgio::SSLConnector
  include Flipper::Base
  class << self
    attr_accessor :ssl_ctx
    attr_accessor :ssl_session
    attr_accessor :tlsext_hostname
  end

  # used for upstream connections
  def self.start
    io = Kgio::Socket.start(@connect_addr)
    new(io, @ssl_ctx, @tlsext_hostname, @ssl_session)
  end
end
