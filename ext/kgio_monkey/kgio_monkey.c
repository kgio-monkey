#include <ruby.h>
#include <limits.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <unistd.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include "ancient_ruby.h"
#include "my_fileno.h"

#ifndef HAVE_SSL_SET_TLSEXT_HOST_NAME
#  define SSL_set_tlsext_host_name(ssl, name) (1)
#endif

VALUE ossl_x509_new(X509 *cert);
extern int ossl_ssl_ex_ptr_idx;

#define KGIO_MONKEY_DONE (SSL_SENT_SHUTDOWN|SSL_RECEIVED_SHUTDOWN)
#define MY_PREAD_MAX 16384

NORETURN(static void raise_empty_bt(VALUE, VALUE));
NORETURN(static void sys_fail(SSL *, const char *, int));
NORETURN(static void ossl_raise_empty_bt(VALUE, const char *));
void ossl_raise(VALUE exc, const char *fmt, ...);
static VALUE sym_wait_readable, sym_wait_writable;
static VALUE cSSLContext, eSSLError;
static VALUE cSSLSession, eSSLSession;
static VALUE mSSL;
static VALUE str_FAILED, str_NONE, str_SUCCESS;
static VALUE str_cache;

static ID iv_to_io, iv_context, iv_kgio_addr, iv_hostname;
static ID id_set_backtrace, id_closed_p;
static ID id_peer_cert;
static ID iv_kgio_connected;
static ID id_verify_certificate_identity;
static VALUE eErrno_EPIPE, eErrno_ECONNRESET, eErrno_ETIMEDOUT;

struct io_args {
	SSL *ssl;
	char *buf;
	int len;
};

static void ssl_gcfree(void *ptr)
{
	if (ptr) SSL_free((SSL *)ptr);
}

static SSL * ssl_get(VALUE self)
{
	SSL *ssl = DATA_PTR(self);

	if (!ssl)
		rb_raise(rb_eIOError, "closed SSL stream");

	return ssl;
}

static void raise_empty_bt(VALUE err, VALUE str)
{
	VALUE exc = rb_exc_new3(err, str);
	VALUE bt = rb_ary_new();

	rb_funcall(exc, id_set_backtrace, 1, bt);
	rb_exc_raise(exc);
}

static void sys_fail(SSL *ssl, const char *msg, int save_errno)
{
	VALUE klass;

	SSL_set_shutdown(ssl, KGIO_MONKEY_DONE);
	switch (save_errno) {
	case EPIPE: klass = eErrno_EPIPE; break;
	case ECONNRESET: klass = eErrno_ECONNRESET; break;
	case ETIMEDOUT: klass = eErrno_ETIMEDOUT; break;
	default:
		errno = save_errno;
		rb_sys_fail(msg);
	}
	errno = 0;
	raise_empty_bt(klass, rb_str_new2(msg));
}

static void ossl_raise_empty_bt(VALUE err, const char *msg)
{
	unsigned long e = ERR_peek_error();
	VALUE str = rb_str_new2(msg);

	if (e) {
		rb_str_cat(str, " (", 2);
		rb_str_cat2(str, ERR_error_string(e, 0));
		rb_str_cat(str, ")", 1);
		ERR_clear_error();
	}
	raise_empty_bt(err, str);
}

static void set_ecdh_curve(SSL_CTX *ctx, const char *name)
{
#if defined(HAVE_OPENSSL_EC_H) && \
    (defined(HAVE_SSL_CTX_SET_TMP_ECDH) || defined(SSL_CTX_set_tmp_ecdh))
	EC_KEY *ecdh;
	int nid = OBJ_sn2nid(name);

	if (nid == 0)
		rb_raise(rb_eArgError, "Unknown curve name: %s", name);
	if (!(ecdh = EC_KEY_new_by_curve_name(nid)))
		ossl_raise(eSSLError, "EC_KEY_new_by_curve_name(%s)", name);
	SSL_CTX_set_tmp_ecdh(ctx, ecdh);
	SSL_CTX_set_options(ctx, SSL_OP_SINGLE_ECDH_USE);
	EC_KEY_free(ecdh);
#else /* old OpenSSL: */
	rb_warn(":ssl_ecdh_curve not supported with old OpenSSL");
#endif
}

/*
 * Fills in some missing parts for nginx compatibility since the
 * "openssl" extension doesn't offer some methods for OpenSSL context
 */
static void ssl_ctx_prepare_options(SSL_CTX *ctx, VALUE o)
{
	VALUE v;
	const char *path;

	assert(TYPE(o) == T_HASH && "not a hash");
	v = rb_hash_aref(o, ID2SYM(rb_intern("ssl_certificate")));
	if (!NIL_P(v)) {
		path = StringValueCStr(v);
		if (! SSL_CTX_use_certificate_chain_file(ctx, path))
			ossl_raise(eSSLError,
			           "SSL_CTX_use_certificate_chain_file");
	}

	/*
	 * OpenSSL::SSL::SSLContext#setup won't setup the "key" attribute
	 * since we used a cert chain file (above) explicitly
	 */
	v = rb_hash_aref(o, ID2SYM(rb_intern("ssl_certificate_key")));
	if (!NIL_P(v)) {
		int type = SSL_FILETYPE_PEM; /* nginx only allows PEM */

		path = StringValueCStr(v);
		if (! SSL_CTX_use_PrivateKey_file(ctx, path, type))
			ossl_raise(eSSLError, "SSL_CTX_use_PrivateKey_file");
	}
	v = rb_hash_aref(o, ID2SYM(rb_intern("ssl_ecdh_curve")));
	if (!NIL_P(v))
		set_ecdh_curve(ctx, StringValueCStr(v));
}

/*
 * call-seq:
 *	Kgio::Monkey!(ssl_ctx, options = {})	-> ssl_ctx
 *
 * Prepares and sets up an existing OpenSSL::SSL::Context object for
 * Kgio::SSL usage. This enables SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER
 * and read-ahead to force OpenSSL to read as much data off the socket
 * as it can.
 *
 * With OpenSSL 1.0.0 or later, this enables SSL_MODE_RELEASE_BUFFERS
 * to save up to 34KB of memory per connection.
 *
 * This calls the OpenSSL::SSL::Context#setup method, which freezes the
 * SSL context and prevents further modification.
 */
static VALUE ssl_ctx_prepare(int argc, VALUE *argv, VALUE self)
{
	SSL_CTX *ssl_ctx;
	VALUE ctx, options;
	long mode;

	rb_scan_args(argc, argv, "11", &ctx, &options);
	if (!rb_obj_is_kind_of(ctx, cSSLContext))
		rb_raise(rb_eTypeError, "not an SSLContext");

	ssl_ctx = DATA_PTR(ctx);
	if (!ssl_ctx)
		rb_raise(rb_eRuntimeError, "SSLContext not initialized");

	switch (TYPE(options)) {
	case T_HASH: ssl_ctx_prepare_options(ssl_ctx, options); break;
	case T_NIL: break;
	default: rb_raise(rb_eTypeError, "not a hash");
	}

	mode = SSL_CTX_get_mode(ssl_ctx);
#ifdef SSL_MODE_RELEASE_BUFFERS
	mode |= SSL_MODE_RELEASE_BUFFERS;
#endif
	mode |= SSL_MODE_ENABLE_PARTIAL_WRITE; /* should be set already */
	mode |= SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER;
	SSL_CTX_set_mode(ssl_ctx, mode);
	SSL_CTX_set_read_ahead(ssl_ctx, 1); /* read as much as possible */

	rb_funcall(ctx, rb_intern("setup"), 0, 0);

	return ctx;
}

static VALUE ssl_alloc(VALUE klass)
{
	return Data_Wrap_Struct(klass, NULL, ssl_gcfree, NULL);
}

/*
 * call-seq:
 *	kgio_ssl.kgio_addr	-> String or nil
 *
 * Returns a string representing the IP address of an accepted socket.
 * This is forwarded to the underlying IO object, assuming it is an
 * Kgio::Socket.  If Kgio::SSL is used to wrap a non-Kgio::Socket
 * object, this will raise NoMethodError
 */
static VALUE kgio_addr(VALUE self)
{
	VALUE io = rb_ivar_get(self, iv_to_io);

	return rb_ivar_get(io, iv_kgio_addr);
}

/*
 * call-seq:
 *	kgio_ssl.closed?	-> true or false
 *
 * Returns true if the kgio_ssl (and associated IO object) is completely
 * closed, false otherwise.
 */
static VALUE closed_p(VALUE self)
{
	if (!DATA_PTR(self))
		return Qtrue;

	return rb_funcall(rb_ivar_get(self, iv_to_io), id_closed_p, 0, 0);
}


/* common to both SSL acceptors and SSL connectors */
static SSL * kgio_ssl_init(VALUE self, VALUE io, VALUE ctx)
{
	SSL *ssl;

	if (!rb_obj_is_kind_of(ctx, cSSLContext))
		rb_raise(rb_eTypeError, "not an SSLContext");
	if (!OBJ_FROZEN(ctx))
		rb_raise(rb_eRuntimeError, "SSLContext not setup");
	if (DATA_PTR(self))
		rb_raise(rb_eRuntimeError, "already initialized");

	ssl = SSL_new(DATA_PTR(ctx)); /* SSL_new() checks ctx != NULL */
	if (!ssl) ossl_raise(eSSLError, "SSL_new");
	DATA_PTR(self) = ssl;

	/*
	 * ctx.servername_cb gets self from ossl_ssl_ex_ptr_idx
	 * and ctx back from self, so we set ossl_ssl_ex_ptr_idx
	 * and iv_context here:
	 */
	if (0 == SSL_set_fd(ssl, my_fileno(io)))
		ossl_raise(eSSLError, "SSL_set_fd");
	if (0 == SSL_set_ex_data(ssl, ossl_ssl_ex_ptr_idx, (void*)self))
		ossl_raise(eSSLError, "SSL_set_ex_data(ptr_idx)");
	rb_ivar_set(self, iv_to_io, io);
	rb_ivar_set(self, iv_context, ctx);

	return ssl;
}

/*
 * call-seq:
 *	Kgio::SSL.new(io, ssl_ctx)
 *
 * Wraps the existing +io+ object with the given OpenSSL::SSL::Context
 * +ssl_ctx+.
 */
static VALUE init(VALUE self, VALUE io, VALUE ctx)
{
	SSL *ssl = kgio_ssl_init(self, io, ctx);

	SSL_set_accept_state(ssl);
	return self;
}



static VALUE read_or_peek(int peek, int argc, VALUE *argv, VALUE self)
{
	VALUE len, buf;
	struct io_args args;
	int rc, save_errno;
	VALUE new_buf = Qfalse;

	rb_scan_args(argc, argv, "11", &len, &buf);
	args.ssl = ssl_get(self);
	args.len = NUM2INT(len);

	if (NIL_P(buf)) {
		new_buf = buf = rb_str_new(0, args.len);
	} else {
		StringValue(buf);
		rb_str_modify(buf);
		rb_str_resize(buf, args.len);
	}
	args.buf = RSTRING_PTR(buf);
	if (args.len == 0)
		return buf;

	if (peek)
		rc = SSL_peek(args.ssl, args.buf, args.len);
	else
		rc = SSL_read(args.ssl, args.buf, args.len);
	if (rc > 0) {
		if (rc != args.len)
			rb_str_set_len(buf, rc);
		return buf;
	}

	save_errno = errno;
	if (new_buf == Qfalse)
		rb_str_set_len(buf, 0);
	else
		rb_str_resize(buf, 0);

	switch (SSL_get_error(args.ssl, rc)) {
	case SSL_ERROR_WANT_WRITE: return sym_wait_writable;
	case SSL_ERROR_WANT_READ: return sym_wait_readable;
	case SSL_ERROR_SYSCALL:
		if (ERR_peek_error() == 0 && rc == 0) {
			SSL_set_shutdown(args.ssl, KGIO_MONKEY_DONE);
			return Qnil;
		}
		if (save_errno)
			sys_fail(args.ssl,
			         peek ? "SSL_peek" : "SSL_read", save_errno);
	case SSL_ERROR_ZERO_RETURN:
		SSL_set_shutdown(args.ssl, KGIO_MONKEY_DONE);
		return Qnil;
	}
	SSL_set_shutdown(args.ssl, KGIO_MONKEY_DONE);
	ossl_raise_empty_bt(eSSLError, peek ? "SSL_peek" : "SSL_read");

	return Qnil;
}

/*
 * call-seq:
 *	kgio_ssl.kgio_tryread(maxlen, buffer = "")   ->  buffer
 *
 * Reads at most maxlen bytes from the stream socket.  Returns with a
 * newly allocated buffer, or may reuse an existing buffer if supplied.
 *
 * Returns nil on EOF.
 *
 * Returns :wait_readable or :wait_writable if EAGAIN is encountered.
 */
static VALUE kgio_tryread(int argc, VALUE *argv, VALUE self)
{
	return read_or_peek(0, argc, argv, self);
}

/*
 * call-seq:
 *	kgio_ssl.kgio_trypeek(maxlen)           ->  buffer
 *	kgio_ssl.kgio_trypeek(maxlen, buffer)   ->  buffer
 *
 * Retrieves at most maxlen bytes from the SSL socket, but preserving
 * it for future peeks or reads.  Returns with a newly allocated buffer,
 * or may reuse an existing buffer if supplied.
 *
 * Returns nil on EOF.
 *
 * Returns :wait_readable or :wait_writable if EAGAIN is encountered.
 */
static VALUE kgio_trypeek(int argc, VALUE *argv, VALUE self)
{
	return read_or_peek(1, argc, argv, self);
}

/*
 * call-seq:
 *	kgio_ssl.kgio_trywrite(str)
 *
 * Returns nil if the write was completed in full.
 *
 * Returns a String containing the unwritten portion if EAGAIN
 * was encountered, but some portion was successfully written.
 *
 * Returns :wait_writable or :wait_readable if EAGAIN is encountered
 * and nothing was written.
 */
static VALUE kgio_trywrite(VALUE self, VALUE str)
{
	int written, wrote_something = 0;
	struct io_args args;

	StringValue(str);
	args.len = RSTRING_LENINT(str);

	if (args.len == 0)
		return Qnil;
	args.buf = RSTRING_PTR(str);
	args.ssl = ssl_get(self);

	while (1) {
		written = SSL_write(args.ssl, args.buf, args.len);
		if (written == args.len) return Qnil;
		if (written > 0) {
			args.buf += written;
			args.len -= written;
			wrote_something = 1;
			continue;
		}

		/* ignore errors until the next call, we wrote something! */
		if (wrote_something)
			return rb_str_new(args.buf, args.len);

		switch (SSL_get_error(args.ssl, written)) {
		case SSL_ERROR_WANT_WRITE: return sym_wait_writable;
		case SSL_ERROR_WANT_READ: return sym_wait_readable;
		case SSL_ERROR_SYSCALL:
			if (errno) sys_fail(args.ssl, "SSL_write", errno);
		}
		SSL_set_shutdown(args.ssl, KGIO_MONKEY_DONE);
		ossl_raise_empty_bt(eSSLError, "SSL_write");
	}
	return Qnil;
}

static VALUE ssl_free_close(VALUE self)
{
	SSL *ssl = ssl_get(self);
	VALUE io = rb_ivar_get(self, iv_to_io);

	DATA_PTR(self) = NULL;
	SSL_free(ssl);

	if (shutdown(my_fileno(io), SHUT_RDWR) != 0)
		errno = 0; /* ignore shutdown() errors */

	return rb_io_close(io);
}

/*
 * call-seq:
 *	kgio_ssl.kgio_tryclose
 *
 * returns +nil+ on success
 * returns +:wait_readable+ or +:wait_writable+ if the operation is in progress
 * returns +false+ on failure
 */
static VALUE kgio_tryclose(VALUE self)
{
	SSL *ssl = ssl_get(self);
	int rc = SSL_shutdown(ssl);
	int state = SSL_get_shutdown(ssl);

	if (rc == 1 || ((state & KGIO_MONKEY_DONE) == KGIO_MONKEY_DONE))
		return ssl_free_close(self);
	if (rc == 0) rc = SSL_shutdown(ssl);
	if (rc == 1)
		return ssl_free_close(self);

	switch (SSL_get_error(ssl, rc)) {
	case SSL_ERROR_WANT_WRITE: return sym_wait_writable;
	case SSL_ERROR_WANT_READ: return sym_wait_readable;
	case SSL_ERROR_ZERO_RETURN:
	case SSL_ERROR_NONE: return ssl_free_close(self);
	}
	ssl_free_close(self);
	return Qfalse;
}

enum sendfile_retval {
	SF_EOF,
	SF_WAIT_READABLE,
	SF_WAIT_WRITABLE,
	SF_WRITE_ERROR,
	SF_OK
};

struct sendfile_args {
	SSL *ssl;
	int in_fd;
	off_t offset;
	off_t count;
	off_t sent_bytes;
};

struct pread_args {
	int fd;
	void *buf;
	size_t count;
	off_t offset;
};

static VALUE nogvl_pread(void *ptr)
{
	struct pread_args *p = ptr;
	ssize_t r = pread(p->fd, p->buf, p->count, p->offset);

	return (VALUE)r;
}

static VALUE do_sendfile(struct sendfile_args *args)
{
	struct pread_args p;
	int w;
	ssize_t r;

	p.fd = args->in_fd;
	p.offset = args->offset;

	if ((args->count < 0) || (args->count > MY_PREAD_MAX))
		p.count = MY_PREAD_MAX;
	else
		p.count = args->count;

	if (p.count == 0)
		return (VALUE)SF_EOF;

	/* xmalloc() can trigger GC too often on MRI */
	p.buf = malloc(p.count);
	if (p.buf == NULL) /* unlikely */
		rb_memerror();

retry_pread:
	r = (ssize_t)rb_thread_blocking_region(nogvl_pread, &p, NULL, 0);
	if (r > 0) {
		w = SSL_write(args->ssl, p.buf, r);

		if (w > 0) {
			p.count -= w;
			args->sent_bytes += w;

			if (w == r && p.count > 0) {
				p.offset += w;
				goto retry_pread;
			}

			free(p.buf);
			return (VALUE)SF_OK;
		}
		free(p.buf);
		/* fall-through to errout */
	} else if (r == 0) {
		free(p.buf);
		return (VALUE)SF_EOF;
	} else {
		if (errno == EINTR) /* NFS may fail with EINTR */
			goto retry_pread;
		w = errno;
		free(p.buf);
		errno = w;
		rb_sys_fail("pread");
	}

errout:
	switch (SSL_get_error(args->ssl, w)) {
	case SSL_ERROR_WANT_WRITE: return (VALUE)SF_WAIT_WRITABLE;
	case SSL_ERROR_WANT_READ: return (VALUE)SF_WAIT_READABLE;
	case SSL_ERROR_SYSCALL:
		w = errno;
		SSL_set_shutdown(args->ssl, KGIO_MONKEY_DONE);
		errno = w;
		return (VALUE)SF_WRITE_ERROR;
	}

	SSL_set_shutdown(args->ssl, KGIO_MONKEY_DONE);
	errno = 0;
	return (VALUE)SF_WRITE_ERROR;
}

/*
 * call-seq:
 *	kgio_ssl.kgio_trysendfile(in_io, offset=0, count=nil)
 *
 * Returns:
 *
 * * Integer on success (number of bytes written)
 * * nil on EOF
 * * :wait_readable or :wait_writable if blocked
 *
 * This API matches the IO#trysendfile in the sendfile RubyGem
 * (except +:wait_readable+ may be returned).
 *
 * Unlike the equivalent C sendfile() function, this never modifies
 * the offset of +in_io+, even if +offset+ is unspecified.
 */
static VALUE kgio_trysendfile(int argc, VALUE *argv, VALUE self)
{
	VALUE in_io, offset, count;
	struct sendfile_args args;
	VALUE rc;

	args.sent_bytes = 0;
	args.ssl = ssl_get(self);
	rb_scan_args(argc, argv, "12", &in_io, &offset, &count);

	if (TYPE(in_io) != T_FILE)
		in_io = rb_convert_type(in_io, T_FILE, "IO", "to_io");
	args.in_fd = my_fileno(in_io);
	args.offset = NIL_P(offset) ? 0 : NUM2OFFT(offset);
	args.count = NIL_P(count) ? (off_t)-1 : NUM2OFFT(count);

	if (args.offset < 0)
		rb_raise(rb_eRuntimeError, "negative offset not allowed");

	rc = do_sendfile(&args);

	if (args.sent_bytes > 0)
		return OFFT2NUM(args.sent_bytes);

	switch ((enum sendfile_retval)rc) {
	case SF_EOF: return Qnil;
	case SF_WAIT_READABLE: return sym_wait_readable;
	case SF_WAIT_WRITABLE: return sym_wait_writable;
	case SF_WRITE_ERROR:
		if (errno) sys_fail(args.ssl, "SSL_write", errno);
		ossl_raise_empty_bt(eSSLError, "SSL_write(trysendfile)");
	case SF_OK:
		assert(0 && "BUG: impossible SF_OK, email kgio@librelist.org");
	}
	return Qnil;
}

/*
 * call-seq:
 *	kgio_ssl.compression -> nil or Symbol
 *
 * Returns the name of the compression used for a given connection as a Symbol.
 * Typical return values are :"zlib compression" or :"run length compression"
 * or +nil+ if no compression is not used.
 */
static VALUE compression(VALUE self)
{
	SSL *ssl = ssl_get(self);
	const COMP_METHOD *comp = SSL_get_current_compression(ssl);

	return comp ? ID2SYM(rb_intern(SSL_COMP_get_name(comp))) : Qnil;
}

/*
 * call-seq:
 *	kgio_ssl.compression?	-> true or false
 *
 * Returns whether or not compression is active for the given Kgio::SSL session.
 */
static VALUE compression_p(VALUE self)
{
	SSL *ssl = ssl_get(self);

	return SSL_get_current_compression(ssl) ? Qtrue : Qfalse;
}

/*
 * call-seq:
 *	kgio_ssl.expansion	-> nil or Symbol
 *
 * Returns the name of the expansion used for a given connection as a Symbol.
 * Typical return values are :"zlib compression" or :"run length compression"
 * or +nil+ if no expansion is not used.
 */
static VALUE expansion(VALUE self)
{
	SSL *ssl = ssl_get(self);
	const COMP_METHOD *comp = SSL_get_current_expansion(ssl);

	return comp ? ID2SYM(rb_intern(SSL_COMP_get_name(comp))) : Qnil;
}

/*
 * call-seq:
 *	kgio_ssl.expansion?	-> true or false
 *
 * Returns whether or not expansion is active for the given Kgio::SSL session.
 */
static VALUE expansion_p(VALUE self)
{
	SSL *ssl = ssl_get(self);

	return SSL_get_current_expansion(ssl) ? Qtrue : Qfalse;
}

/*
 * call-seq:
 *	kgio_ssl.session_reused?	-> true or false
 *
 * Returns whether or not the session was reused.
 */
static VALUE session_reused_p(VALUE self)
{
	SSL *ssl = ssl_get(self);

	return SSL_session_reused(ssl) ? Qtrue : Qfalse;
}

/*
 * call-seq:
 *	kgio_ssl.session	-> OpenSSL::SSL::SSLSession
 *
 * Returns the SSLSession object associated with the given Kgio::SSL object
 */
static VALUE session_get(VALUE self)
{
	SSL *ssl = ssl_get(self);
	SSL_SESSION *sess = SSL_get1_session(ssl);
	VALUE rv;

	if (!sess)
		ossl_raise(eSSLSession, "no session available");

	rv = rb_obj_alloc(cSSLSession);
	DATA_PTR(rv) = sess;
	return rv;
}

/*
 * call-seq:
 *    kgio_ssl.peer_cert => cert or nil
 *
 * Returns the peer certificate for this Kgio::SSL object (for client sockets)
 */
static VALUE peer_cert(VALUE self)
{
	SSL *ssl = ssl_get(self);
	X509 *cert;
	VALUE rv;

	rv = rb_attr_get(self, id_peer_cert);
	if (!NIL_P(rv))
		return rv;
	cert = SSL_get_peer_certificate(ssl);
	if (!cert)
		return Qnil;

	rv = ossl_x509_new(cert);
	X509_free(cert);

	return rb_ivar_set(self, id_peer_cert, rv);
}

/*
 * Returns a frozen string given a constant pointer to name or nil if name
 * is NULL.  We actually use the pointer address to hash this value since
 * OpenSSL (rightfully) returns the same address in memory every time.
 */
static VALUE frozen_or_nil(const char *name)
{
	VALUE rv, key;

	if (!name)
		return Qnil;

	key = ULONG2NUM((unsigned long)name);
	rv = rb_hash_aref(str_cache, key);
	if (!NIL_P(rv))
		return rv;

	rv = rb_str_new2(name);
	return rb_hash_aset(str_cache, key, rb_obj_freeze(rv));
}

/*
 * call-seq:
 * 	kgio_ssl.ssl_cipher	-> String
 *
 * Returns the name of the cipher used in the connection as a frozen
 * string.
 */
static VALUE ssl_cipher(VALUE self)
{
	return frozen_or_nil(SSL_get_cipher_name(ssl_get(self)));
}

/*
 * call-seq:
 * 	kgio_ssl.ssl_protocol	-> "SSLv2", "SSLv3", or "TLSv1"
 *
 * Returns the name of the protocol version of a connection as a frozen
 * string.  May return "unknown" if no connection has been established.
 */
static VALUE ssl_protocol(VALUE self)
{
	return frozen_or_nil(SSL_get_version(ssl_get(self)));
}

/*
 * call-seq:
 *
 *	kgio_ssl.ssl_client_verify	-> "SUCCESS", "NONE", or "FAILED"
 *
 * Returns "SUCCESS" if client verification failed, "NONE" if no peer
 * certificate was provided, and "FAILED" if certificate verification
 * failed.
 *
 * This matches the $ssl_client_verify variable supported by nginx.
 */
static VALUE ssl_client_verify(VALUE self)
{
	SSL *ssl = ssl_get(self);

	if (SSL_get_verify_result(ssl) != X509_V_OK)
		return str_FAILED;
	return NIL_P(peer_cert(self)) ? str_NONE : str_SUCCESS;
}

static VALUE post_connection_check(VALUE self)
{
	VALUE hostname = rb_ivar_get(self, iv_hostname);
	VALUE rv = rb_funcall(mSSL, id_verify_certificate_identity, 2,
	                      peer_cert(self), hostname);
	if (rv == Qfalse) {
		SSL_set_shutdown(ssl_get(self), KGIO_MONKEY_DONE);
		rb_raise(eSSLError,
			"hostname=%s does not match server certificate",
			StringValueCStr(hostname));
	}

	return rb_ivar_set(self, iv_kgio_connected, Qtrue);
}

static VALUE conn_tryconnect(VALUE self)
{
	SSL *ssl = ssl_get(self);
	int rc = SSL_connect(ssl);

	if (rc == 1)
		return post_connection_check(self); /* Qtrue or raises */
	if (rc < 0) {
		switch (SSL_get_error(ssl, rc)) {
		case SSL_ERROR_WANT_WRITE: return sym_wait_writable;
		case SSL_ERROR_WANT_READ: return sym_wait_readable;
		}
	}
	SSL_set_shutdown(ssl, KGIO_MONKEY_DONE);
	ossl_raise(eSSLError, "SSL_connect");
	return Qfalse;
}

static VALUE conn_check_connected(VALUE self)
{
	VALUE connected_p = rb_ivar_get(self, iv_kgio_connected);

	if (connected_p != Qfalse)
		return Qtrue;
	return conn_tryconnect(self);
}

#define RETURN_WAIT_UNLESS_CONNECTED(self) { \
	VALUE check = conn_check_connected((self)); \
	if (SYMBOL_P(check)) return check; \
} while (0);

/* :nodoc: */
static VALUE conn_tryread(int argc, VALUE *argv, VALUE self)
{
	RETURN_WAIT_UNLESS_CONNECTED(self);
	return kgio_tryread(argc, argv, self);
}

/* :nodoc: */
static VALUE conn_trypeek(int argc, VALUE *argv, VALUE self)
{
	RETURN_WAIT_UNLESS_CONNECTED(self);
	return kgio_trypeek(argc, argv, self);
}

/* :nodoc: */
static VALUE conn_trywrite(VALUE self, VALUE str)
{
	RETURN_WAIT_UNLESS_CONNECTED(self);
	return kgio_trywrite(self, str);
}

/* :nodoc: */
static VALUE conn_trysendfile(int argc, VALUE *argv, VALUE self)
{
	RETURN_WAIT_UNLESS_CONNECTED(self);
	return kgio_trysendfile(argc, argv, self);
}

/*
 * call-seq:
 *	Kgio::SSLConnector.new(io, ssl_ctx, hostname, session = nil)
 *
 * Initializes an SSL/TLS client socket.  Like Kgio::SSL.new, except
 * +hostname+ is required for verification and +session+ may be specified
 * as an OpenSSL::SSL::Session object.
 */
static VALUE conn_init(int argc, VALUE *argv, VALUE self)
{
	VALUE io, ctx, hostname, session;
	SSL *ssl;

	rb_scan_args(argc, argv, "31", &io, &ctx, &hostname, &session);
	ssl = kgio_ssl_init(self, io, ctx);

	if (SSL_set_tlsext_host_name(ssl, StringValueCStr(hostname)) != 1)
		ossl_raise(eSSLError, "SSL_set_tlsext_host_name");

	if (rb_obj_is_kind_of(session, cSSLSession)) {
		SSL_SESSION *sess = DATA_PTR(session);
		if (!sess)
			ossl_raise(eSSLSession, "no session available");
		if (SSL_set_session(ssl, sess) != 1)
			ossl_raise(eSSLError, "SSL_set_session");
	}
	rb_ivar_set(self, iv_hostname, hostname);
	rb_ivar_set(self, iv_kgio_connected, Qfalse);

	return conn_tryconnect(self);
}

/*
 * call-seq:
 *
 *	Kgio::SSL.compression = false
 *
 * Setting this to +false+ disables compression globally within the process.
 * Other values are currently not supported, but may be in the future.
 * This affects /ALL/ OpenSSL connections within the process; OpenSSL
 * currently (1.0.0d) does not provide a way to enable/disable compression
 * on a per-context or per-connection basis.
 *
 * Disabling compression can significantly reduce memory usage of
 * idle connections.
 *
 * Newer versions of OpenSSL supports the Kgio::SSL::OP_NO_COMPRESSION
 * constant which allows disabling compression on a per-SSLContext basis.
 * OpenSSL::SSL::OP_NO_COMPRESSION should also be supported in future
 * versions of Ruby (1.9.3+).
 */
static VALUE set_compression(VALUE self, VALUE setting)
{
	STACK_OF(SSL_COMP)* comp_methods = SSL_COMP_get_compression_methods();

	if (setting == Qfalse || NIL_P(setting))
		sk_SSL_COMP_zero(comp_methods);
	else
		rb_raise(rb_eArgError, "compression setting must be `false'");

	return setting;
}

void Init_kgio_monkey_ext(void)
{
	VALUE mKgio, mOpenSSL;
	VALUE cKgioSSL, cConnector;

	/*
	 * See http://bogomips.org/kgio/ for information on Kgio
	 */
	mKgio = rb_define_module("Kgio");
	mOpenSSL = rb_const_get(rb_cObject, rb_intern("OpenSSL"));
	mSSL = rb_const_get(mOpenSSL, rb_intern("SSL"));
	cSSLContext = rb_const_get(mSSL, rb_intern("SSLContext"));
	eSSLError = rb_const_get(mSSL, rb_intern("SSLError"));
	cSSLSession = rb_const_get(mSSL, rb_intern("Session"));
	eSSLSession = rb_const_get(cSSLSession, rb_intern("SessionError"));

	rb_define_singleton_method(mKgio, "Monkey!", ssl_ctx_prepare, -1);

	/*
	 * Document-class: Kgio::SSL
	 *
	 * This class wraps an existing IO object and provides it
	 * SSL/TLS methods.  This is only intended for servers
	 * that accept SSL/TLS connections.
	 */
	cKgioSSL = rb_define_class_under(mKgio, "SSL", rb_cObject);

	/*
	 * Document-class: Kgio::SSLConnector
	 *
	 * Like Kgio::SSL, but for SSL/TLS clients that connect to
	 * servers, not for SSL/TLS servers.
	 */
	cConnector = rb_define_class_under(mKgio, "SSLConnector", cKgioSSL);
	rb_define_alloc_func(cKgioSSL, ssl_alloc);
	rb_define_method(cKgioSSL, "kgio_tryread", kgio_tryread, -1);
	rb_define_method(cKgioSSL, "kgio_trypeek", kgio_trypeek, -1);
	rb_define_method(cKgioSSL, "kgio_trywrite", kgio_trywrite, 1);
	rb_define_method(cKgioSSL, "kgio_trysendfile", kgio_trysendfile, -1);

	rb_define_method(cConnector, "kgio_tryread", conn_tryread, -1);
	rb_define_method(cConnector, "kgio_trypeek", conn_trypeek, -1);
	rb_define_method(cConnector, "kgio_trywrite", conn_trywrite, 1);
	rb_define_method(cConnector, "kgio_trysendfile", conn_trysendfile, -1);

	rb_define_method(cKgioSSL, "kgio_addr", kgio_addr, 0);
	rb_define_method(cKgioSSL, "closed?", closed_p, 0);

	rb_define_method(cKgioSSL, "compression", compression, 0);
	rb_define_method(cKgioSSL, "expansion", expansion, 0);
	rb_define_method(cKgioSSL, "compression?", compression_p, 0);
	rb_define_method(cKgioSSL, "expansion?", expansion_p, 0);
	rb_define_method(cKgioSSL, "session_reused?", session_reused_p, 0);
	rb_define_method(cKgioSSL, "session", session_get, 0);
	rb_define_method(cKgioSSL, "peer_cert", peer_cert, 0);
	rb_define_method(cKgioSSL, "ssl_cipher", ssl_cipher, 0);
	rb_define_method(cKgioSSL, "ssl_protocol", ssl_protocol, 0);
	rb_define_method(cKgioSSL, "ssl_client_verify", ssl_client_verify, 0);

	rb_define_singleton_method(cKgioSSL, "compression=",
	                           set_compression, 1);

#ifdef SSL_OP_NO_COMPRESSION
	/*
	 * Disables compression for a given SSL context, potentially
	 * resulting in reduced memory usage.  This is identical to
	 * the OpenSSL::SSL::OP_NO_COMPRESSION constant that appears
	 * in Ruby 1.9.3.  Not all versions of OpenSSL support this
	 * feature.
	 */
	rb_define_const(cKgioSSL, "OP_NO_COMPRESSION",
	                UINT2NUM(SSL_OP_NO_COMPRESSION));
#endif

	/*
	 * provides access to the underlying IO object, used internally by
	 * IO.select and Kgio.poll
	 */
	rb_define_attr(cKgioSSL, "to_io", 1, 0);
	rb_define_private_method(cKgioSSL, "initialize", init, 2);
	rb_define_private_method(cConnector, "initialize", conn_init, -1);
	rb_define_method(cKgioSSL, "kgio_tryclose", kgio_tryclose, 0);

	sym_wait_readable = ID2SYM(rb_intern("wait_readable"));
	sym_wait_writable = ID2SYM(rb_intern("wait_writable"));
	id_set_backtrace = rb_intern("set_backtrace");
	id_closed_p = rb_intern("closed?");
	id_verify_certificate_identity =
				rb_intern("verify_certificate_identity");
	eErrno_EPIPE = rb_const_get(rb_mErrno, rb_intern("EPIPE"));
	eErrno_ECONNRESET = rb_const_get(rb_mErrno, rb_intern("ECONNRESET"));
	eErrno_ETIMEDOUT = rb_const_get(rb_mErrno, rb_intern("ETIMEDOUT"));
	iv_to_io = rb_intern("@to_io");
	iv_context = rb_intern("@context");
	iv_kgio_addr = rb_intern("@kgio_addr");
	iv_hostname = rb_intern("@hostname");
	iv_kgio_connected = rb_intern("@kgio_connected");
	id_peer_cert = rb_intern("peer_cert");

	rb_global_variable(&str_cache);
	str_cache = rb_hash_new();
	str_FAILED = frozen_or_nil("FAILED");
	str_NONE = frozen_or_nil("NONE");
	str_SUCCESS = frozen_or_nil("SUCCESS");
}
