#ifndef MISSING_ANCIENT_RUBY_H
#define MISSING_ANCIENT_RUBY_H

#ifndef HAVE_RB_STR_SET_LEN
static void my_str_set_len(VALUE str, long len)
{
	RSTRING(str)->len = len;
	RSTRING(str)->ptr[len] = '\0';
}
#define rb_str_set_len(str,len) my_str_set_len((str),(len))
#endif /* ! HAVE_RB_STR_SET_LEN */

#ifndef RSTRING_PTR
#  define RSTRING_PTR(s) (RSTRING(s)->ptr)
#endif /* !defined(RSTRING_PTR) */
#ifndef RSTRING_LEN
#  define RSTRING_LEN(s) (RSTRING(s)->len)
#endif /* !defined(RSTRING_LEN) */

#ifndef RSTRING_LENINT /* TODO: handle overflow */
#  define RSTRING_LENINT(str) ((int)RSTRING_LEN((str)))
#endif

#ifndef HAVE_RB_THREAD_BLOCKING_REGION
/* (very) partial emulation of the 1.9 rb_thread_blocking_region under 1.8 */
#  include <rubysig.h>
#  define RUBY_UBF_IO ((rb_unblock_function_t *)-1)
typedef void rb_unblock_function_t(void *);
typedef VALUE rb_blocking_function_t(void *);
static VALUE
my_thread_blocking_region(
	rb_blocking_function_t *fn, void *data1,
	rb_unblock_function_t *ubf, void *data2)
{
	VALUE rv;

	/* allow interrupting pread() on NFS */
	TRAP_BEG;
	rv = fn(data1);
	TRAP_END;

	return rv;
}

#define rb_thread_blocking_region(a,b,c,d) \
        my_thread_blocking_region((a),(b),(c),(d))
#endif /* ! HAVE_RB_THREAD_BLOCKING_REGION */

#endif /* MISSING_ANCIENT_RUBY_H */
