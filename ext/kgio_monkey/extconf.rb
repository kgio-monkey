require "mkmf"
dir_config("openssl")
dir_config("kerberos")
have_header("openssl/ssl.h")
have_library("ssl")
have_library("crypto")
have_func("rb_thread_blocking_region")
have_func("ossl_raise")
have_func("rb_str_set_len")
have_header("openssl/ec.h")
unless have_func("SSL_set_tlsext_host_name", 'openssl/ssl.h')
  have_macro("SSL_set_tlsext_host_name",  'openssl/ssl.h') and
    $defs.push("-DHAVE_SSL_SET_TLSEXT_HOST_NAME")
end
have_func("SSL_CTX_set_tmp_ecdh", 'openssl/ssl.h')
create_makefile("kgio_monkey_ext")
