# -*- encoding: binary -*-
require "test/unit"
require "kgio/monkey"
require "./test/common.rb"
require "pp"
require "tempfile"

class TestKgioComp < Test::Unit::TestCase
  include MonkeyCommon

  def setup
    @to_close = []
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @s_ctx = OpenSSL::SSL::SSLContext.new
    @s_ctx.key = OpenSSL::PKey::RSA.new(IO.read("server.key"))
    @s_ctx.cert = OpenSSL::X509::Certificate.new(IO.read("server.crt"))
    assert_equal @s_ctx, Kgio::Monkey!(@s_ctx)
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @s_tcp.ssl_ctx = @s_ctx
    @port = @s_tcp.addr[1]
    @to_close << @s_tcp
    @client_ctx = OpenSSL::SSL::SSLContext.new
    @client_ctx.ca_file = "ca.crt"
    @client_ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    Kgio::Monkey!(@client_ctx)
  end

  def test_compression_setting_bad
    assert_raises(ArgumentError) { Kgio::SSL.compression = 666 }
    thr = Thread.new do
      sock = Kgio::TCPSocket.new(@host, @port)
      sock = Kgio::SSLConnector.new(sock, @client_ctx, @host)
      sock.kgio_write "HI"
      sock
    end
    client = @s_tcp.kgio_accept
    assert_equal "HI", client.kgio_read(2)
    assert_equal client.compression?, thr.value.compression?
  end

  def test_compression_disable
    pid = fork do
      assert_equal(nil, Kgio::SSL.compression = nil)
      assert_equal(false, Kgio::SSL.compression = false)
      client = @s_tcp.kgio_accept
      assert_equal "HI", client.kgio_read(2)
      assert_equal false, client.compression?
    end
    sock = Kgio::TCPSocket.new(@host, @port)
    sock = Kgio::SSLConnector.new(sock, @client_ctx, @host)
    sock.kgio_write "HI"
    assert_equal false, sock.compression?
    _, status = Process.waitpid2(pid)
    assert status.success?, status.inspect
  end

  def test_compression_per_context
    @client_ctx = OpenSSL::SSL::SSLContext.new
    @client_ctx.ca_file = "ca.crt"
    @client_ctx.options = OpenSSL::SSL::OP_ALL | Kgio::SSL::OP_NO_COMPRESSION
    @client_ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    Kgio::Monkey!(@client_ctx)
    thr = Thread.new do
      client = @s_tcp.kgio_accept
      assert_equal "HI", client.kgio_read(2)
      assert_equal false, client.compression?
      true
    end
    sock = Kgio::TCPSocket.new(@host, @port)
    sock = Kgio::SSLConnector.new(sock, @client_ctx, @host)
    sock.kgio_write "HI"
    assert_equal false, sock.compression?
    assert_equal true, thr.value
  end if OpenSSL::OPENSSL_VERSION_NUMBER >= 0x10000000

  def test_compression
    cmd = "openssl s_client " \
          "-connect #@host:#@port -CAfile ca.crt -reconnect"
    orig_err = $stderr.dup
    accepted = []
    data = []
    Thread.new do
      loop do
        client = @s_tcp.kgio_accept
        accepted << client
        client.kgio_write("HI")
        info = {
          :compression => client.compression,
          :compression? => client.compression?,
          :expansion => client.expansion,
          :expansion? => client.expansion?,
        }
        data << info
        client.kgio_close
      end
    end
    $stderr.reopen(tmp = Tempfile.new("tmperr"))
    o = begin
      `#{cmd}`
    ensure
      $stderr.reopen(orig_err)
    end
    o = o.split(/\n/)

    compression = o.grep(/^Compression:/)
    if compression.empty?
      warn "openssl s_client does not support compression"
    else
      pp [:compression_data, data ] if $DEBUG
      assert_equal data[1][:compression].to_s, compression[1].split(/:\s*/)[1]
      assert data[1][:compression?]
    end

    expansion = o.grep(/^Expansion:/)
    if expansion.empty?
      warn "openssl s_client does not support expansion"
    else
      pp [:expansion_data, data ] if $DEBUG
      assert_equal data[1][:expansion].to_s, expansion[1].split(/:\s*/)[1]
      assert data[1][:expansion?]
    end

    data.clear

    $stderr.reopen(tmp = Tempfile.new("tmperr"))
    o = begin
      `#{cmd} -no_comp`
    ensure
      $stderr.reopen(orig_err)
    end
    o = o.split(/\n/)

    compression = o.grep(/^Compression:/)
    unless compression.empty?
      pp [:compression_data, data ] if $DEBUG
      assert_nil data[1][:compression]
      assert_equal "NONE", compression[1].split(/:\s*/)[1]
      assert ! data[1][:compression?]
    end

    expansion = o.grep(/^Expansion:/)
    unless expansion.empty?
      pp [:expansion_data, data ] if $DEBUG
      assert_nil data[1][:expansion]
      assert_equal "NONE", expansion[1].split(/:\s*/)[1]
      assert ! data[1][:expansion?]
    end
  end
end
