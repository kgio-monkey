# -*- encoding: binary -*-
require "test/unit"
require "flipper"
Thread.abort_on_exception = true

class TestServernameCb < Test::Unit::TestCase
  class BadHostName < RuntimeError
  end

  def setup
    @opts = {
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :flipper_sni => Flipper::SNI.new(proc { |h| raise BadHostName, h }),
      :ssl_version => :TLSv1,
    }
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @port = @s_tcp.addr[1]
    @c_ctx = OpenSSL::SSL::SSLContext.new(:TLSv1)
    @c_ctx.ca_file = "server.crt"
  end

  def test_regexp
    @opts[:sni_hostnames] = /\.example\.com\z/i
    @s_tcp.ssl_ctx = Flipper.ssl_context(@opts)

    sock = TCPSocket.new(@host, @port)
    ssl = OpenSSL::SSL::SSLSocket.new(sock, @c_ctx)
    ssl.hostname = "666.example.com"
    thr = Thread.new { ssl.connect }
    accepted = @s_tcp.kgio_accept
    accepted.kgio_write "HI"
    thr.join
    assert_equal "HI", ssl.read(2)

    sock = TCPSocket.new(@host, @port)
    ssl = OpenSSL::SSL::SSLSocket.new(sock, @c_ctx)
    ssl.hostname = "666.EXAMPLE.com"
    thr = Thread.new { ssl.connect }
    accepted = @s_tcp.kgio_accept
    accepted.kgio_write "HI"
    thr.join
    assert_equal "HI", ssl.read(2)

    sock = TCPSocket.new(@host, @port)
    ssl = OpenSSL::SSL::SSLSocket.new(sock, @c_ctx)
    ssl.hostname = "666.example.con"
    thr = Thread.new do
      begin
        ssl.connect
      rescue => e
        e
      end
    end
    accepted = @s_tcp.kgio_accept
    assert_raises(OpenSSL::SSL::SSLError) { accepted.kgio_write "HI" }
    assert_nil accepted.kgio_tryclose
    assert_kind_of OpenSSL::SSL::SSLError, thr.value
  end

  def test_exact
    @opts[:sni_hostnames] = %w(666.example.com)
    @s_tcp.ssl_ctx = Flipper.ssl_context(@opts)

    sock = TCPSocket.new(@host, @port)
    ssl = OpenSSL::SSL::SSLSocket.new(sock, @c_ctx)
    ssl.hostname = "666.example.com"
    thr = Thread.new { ssl.connect }
    accepted = @s_tcp.kgio_accept
    accepted.kgio_write "HI"
    thr.join
    assert_equal "HI", ssl.read(2)

    sock = TCPSocket.new(@host, @port)
    ssl = OpenSSL::SSL::SSLSocket.new(sock, @c_ctx)
    ssl.hostname = "666.EXAMPLE.com"
    thr = Thread.new { ssl.connect }
    accepted = @s_tcp.kgio_accept
    accepted.kgio_write "HI"
    thr.join
    assert_equal "HI", ssl.read(2)

    sock = TCPSocket.new(@host, @port)
    ssl = OpenSSL::SSL::SSLSocket.new(sock, @c_ctx)
    ssl.hostname = "666.example.con"
    thr = Thread.new do
      begin
        ssl.connect
      rescue => e
        e
      end
    end
    accepted = @s_tcp.kgio_accept
    assert_raises(OpenSSL::SSL::SSLError) { accepted.kgio_write "HI" }
    assert_nil accepted.kgio_tryclose
    assert_kind_of OpenSSL::SSL::SSLError, thr.value
  end
end if OpenSSL::SSL::SSLContext.instance_methods.include?(:servername_cb)
