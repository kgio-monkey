require "test/unit"
require "kgio/monkey"
require "tempfile"
class TestSSLCertSplit < Test::Unit::TestCase
  def test_catted
    cert_class = OpenSSL::X509::Certificate
    tmp = Tempfile.new(%w(catted pem))
    tmp.syswrite(IO.read("client1.crt"))
    tmp.syswrite(IO.read("client2.crt"))
    tmp.syswrite(IO.read("ca.crt"))
    path = tmp.path
    client1, client2, ca = Kgio::SSL.split_pem(cert_class, path)
    assert_equal cert_class.new(IO.read("client1.crt")).to_pem, client1.to_pem
    assert_equal cert_class.new(IO.read("client2.crt")).to_pem, client2.to_pem
    assert_equal cert_class.new(IO.read("ca.crt")).to_pem, ca.to_pem
  end

  def test_single
    cert_class = OpenSSL::X509::Certificate
    client1, client2, ca = Kgio::SSL.split_pem(cert_class, "client1.crt")
    assert_equal cert_class.new(IO.read("client1.crt")).to_pem, client1.to_pem
  end
end
