# -*- encoding: binary -*-
require "test/unit"
require "tempfile"
require "kgio/monkey"
require "./test/common.rb"

class TestSessionCache < Test::Unit::TestCase
  include MonkeyCommon

  def setup
    @to_close = []
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @s_ctx = OpenSSL::SSL::SSLContext.new
    @s_ctx.session_cache_mode = OpenSSL::SSL::SSLContext::SESSION_CACHE_SERVER
    @s_ctx.key = OpenSSL::PKey::RSA.new(IO.read("server.key"))
    @s_ctx.cert = OpenSSL::X509::Certificate.new(IO.read("server.crt"))
    assert_equal @s_ctx, Kgio::Monkey!(@s_ctx)
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @s_tcp.ssl_ctx = @s_ctx
    @port = @s_tcp.addr[1]
    @to_close << @s_tcp
  end

  def test_session_cache
    orig_err = $stderr.dup
    accepted = []
    Thread.new {
      client = @s_tcp.kgio_accept
      accepted << client

      case rc = client.kgio_trywrite("HI")
      when Symbol
        poll(client => rc)
      when String
        assert false, "socket buffer couldn't even write 'HI'"
      else
        ensure_close(client)
        client = @s_tcp.kgio_accept
        accepted << client
      end while true
    }
    $stderr.reopen(tmp = Tempfile.new("tmperr"))
    o = begin
      `openssl s_client -connect #@host:#@port -CAfile ca.crt -reconnect`
    ensure
      $stderr.reopen(orig_err)
    end
    o = o.split(/\n/)
    reused_count = o.grep(/^Reused/).size
    p [ :reused, reused_count, o.grep(/^Reused/) ] if $DEBUG
    if o.grep(/Secure Renegotiation/).size > 0
      tmp.rewind
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
  end

  def test_session_cache_client
    accepted = []
    Thread.new {
      client = @s_tcp.kgio_accept
      accepted << client

      case rc = client.kgio_trywrite("HI")
      when Symbol
        poll(client => rc)
      when String
        assert false, "socket buffer couldn't even write 'HI'"
      else
        ensure_close(client)
        client = @s_tcp.kgio_accept
        accepted << client
      end while true
    }
    ctx = OpenSSL::SSL::SSLContext.new("TLSv1")
    ctx.ca_file = "ca.crt"
    ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    Kgio::Monkey!(ctx)

    sock = Kgio::TCPSocket.new(@host, @port)
    ssl = Kgio::SSLConnector.new(sock, ctx, @host)
    assert_equal "HI", ssl.kgio_read(666)
    session = ssl.session
    assert_kind_of OpenSSL::SSL::Session, session
    assert ! ssl.session_reused?, "session reuse unexpected"
    assert_nil ssl.kgio_close

    sock = Kgio::TCPSocket.new(@host, @port)
    ssl = Kgio::SSLConnector.new(sock, ctx, @host, session)
    assert_equal "HI", ssl.kgio_read(666)
    assert ssl.session_reused?, "session not reused"
    assert_equal session.to_text, ssl.session.to_text
    assert_nil ssl.kgio_close
  end
end
