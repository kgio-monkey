# -*- encoding: binary -*-
require "test/unit"
require "tempfile"
require "flipper"
require "./test/memcached"
begin
  require "memcached"
rescue LoadError
end

class TestMemcachedSessionCache < Test::Unit::TestCase
  include TestMemcached

  def test_session_cache_memcached_openssl
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => "memcached:[#@host:#@mc_port]"
      )
    accepted = server_thread
    o, reused_count, tmp = run_openssl
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end

    sess = Tempfile.new("sess")
    o, reused_count_out, tmp = run_openssl(["-sess_out", sess.path])
    o, reused_count_in, tmp = run_openssl(["-sess_in", sess.path])
    assert_equal reused_count_in, reused_count_out + 1
    assert reused_count_in > 0

    session = OpenSSL::SSL::Session.new(sess.read)
    assert ctx.session_remove_cb.call(session)
    assert ! ctx.session_remove_cb.call(session)
    o, reused_count_in2, tmp = run_openssl(["-sess_in", sess.path])
    assert_equal reused_count_out, reused_count_in2
  end

  def test_session_cache_memcached_timeout_openssl
    @s_tcp.ssl_ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => [
        :memcached, "#@host:#@mc_port", { :timeout => 0.1 }
      ]
    )
    accepted = server_thread
    o, reused_count, tmp = run_openssl
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
  end

  def test_session_cache_builtin_memcached_with_timeouts_openssl
    @s_tcp.ssl_ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => [
        "builtin:666",
        [ :memcached, [ "#@host:#@mc_port" ], { :timeout=>666} ]
      ]
    )
    accepted = server_thread
    o, reused_count, tmp = run_openssl
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
  end

  def test_memcached_timeout_1s_openssl
    sess = Tempfile.new('sess')
    @s_tcp.ssl_ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => "memcached:[#@host:#@mc_port]",
      :ssl_session_timeout => 1
      )
    accepted = server_thread
    o, reused_count, tmp = run_openssl(['-sess_out', sess.path])
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
    session_id_a = o.grep(/Session-ID:/).uniq[0]
    sleep 2
    o, reused_count2, tmp = run_openssl(['-sess_in', sess.path])
    session_id_b = o.grep(/Session-ID:/).uniq[0]
    assert session_id_a != session_id_b
    assert_equal reused_count, reused_count2
  end
end if defined?(TestMemcached) && TestMemcached::MEMCACHED_PATH &&
       defined?(Memcached)
