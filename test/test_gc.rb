# -*- encoding: binary -*-
require "test/unit"
require "kgio/monkey"
require "tempfile"
require "./test/common"

class TestGC < Test::Unit::TestCase
  def setup
    @to_close = []
    @s_ctx = OpenSSL::SSL::SSLContext.new
    @s_ctx.key = OpenSSL::PKey::RSA.new(IO.read("server.key"))
    @s_ctx.cert = OpenSSL::X509::Certificate.new(IO.read("server.crt"))
    assert_equal @s_ctx, Kgio::Monkey!(@s_ctx)

    @tmp = Tempfile.new("kgio_monkey_test_gc")
    @tmp_path = @tmp.path
    File.unlink(@tmp_path)
    @srv = Kgio::UNIXServer.new(@tmp_path)
    @srv.extend Kgio::SSLServer
    @srv.ssl_ctx = @s_ctx
    @to_close << @srv
  end

  def teardown
    trap(:CHLD, "DEFAULT")
  end

  def test_accept_gc
    running = true
    trap(:CHLD) { running = false }
    err = Tempfile.new("stderr")
    nr = 100000
    r, w = IO.pipe
    rr, ww = IO.pipe
    r.sync = w.sync = true
    rr.sync = ww.sync = true
    @to_close << r << w
    pid = fork do
      $stderr.reopen(err.path)
      nr.times do
        io = @srv.kgio_accept
        assert_kind_of Kgio::SSL, io
      end
      @srv.close
      File.unlink(@tmp_path)
      w.write "."
      rr.read(1)
    end
    @srv.close
    begin
      UNIXSocket.new(@tmp_path)
    rescue
      break
    end while true
    assert_equal ".", r.read(1)
    status = "/proc/#{pid}/status"
    warn(File.read(status)) if File.readable?(status) && $DEBUG
    ww.write(".")
    _, status = Process.waitpid2(pid)
    assert status.success?, status.inspect
    assert_equal 0, err.size
  end

  def teardown
    @to_close.each { |io| io.close unless io.closed? }
  end
end
