require "test/unit"
require "flipper"
require "tempfile"

class Test::FlipperContext < Test::Unit::TestCase
  include OpenSSL::SSL

  def test_protocols
    ctx = Flipper.ssl_context(:ssl_protocols => %w(TLSv1))
    assert_equal(OP_NO_SSLv2, ctx.options & OP_NO_SSLv2)
    assert_equal(OP_NO_SSLv3, ctx.options & OP_NO_SSLv3)
    assert_equal(0, ctx.options & OP_NO_TLSv1)

    ctx = Flipper.ssl_context(:ssl_protocols => %w(SSLv2))
    assert_equal(0, ctx.options & OP_NO_SSLv2)
    assert_equal(OP_NO_SSLv3, ctx.options & OP_NO_SSLv3)
    assert_equal(OP_NO_TLSv1, ctx.options & OP_NO_TLSv1)

    ctx = Flipper.ssl_context(:ssl_protocols => %w(SSLv2 SSLv3))
    assert_equal(0, ctx.options & OP_NO_SSLv2)
    assert_equal(0, ctx.options & OP_NO_SSLv3)
    assert_equal(OP_NO_TLSv1, ctx.options & OP_NO_TLSv1)

    ctx = Flipper.ssl_context(:ssl_protocols => %w(SSLv2 SSLv3 TLSv1))
    assert_equal(0, ctx.options & OP_NO_SSLv2)
    assert_equal(0, ctx.options & OP_NO_SSLv3)
    assert_equal(0, ctx.options & OP_NO_TLSv1)

    ctx = Flipper.ssl_context({})
    assert_equal(0, ctx.options & OP_NO_SSLv2)
    assert_equal(0, ctx.options & OP_NO_SSLv3)
    assert_equal(0, ctx.options & OP_NO_TLSv1)
  end

  def test_server_certs
    o = { :ssl_certificate => "server.crt",
          :ssl_certificate_key => "server.key" }.freeze
    ctx = Flipper.ssl_context(o)
    assert_instance_of OpenSSL::SSL::SSLContext, ctx
    assert ctx.frozen?
  end

  def test_dhparam
    dh = OpenSSL::PKey::DH.new(128)
    tmp = Tempfile.new("dhparam")
    expect = dh.to_s.freeze
    tmp.write(expect)
    tmp.flush
    ctx = Flipper.ssl_context(:ssl_dhparam => tmp.path)
    got = ctx.tmp_dh_callback.call(6,6,6)
    assert_equal expect.chomp, got.to_s.chomp
    assert_instance_of OpenSSL::PKey::DH, got
  end

  def test_ecdh_curve
    assert_raises(ArgumentError) do
      Flipper.ssl_context(:ssl_ecdh_curve => "curvaceous")
    end
    assert_instance_of OpenSSL::SSL::SSLContext,
                       Flipper.ssl_context(:ssl_ecdh_curve => "prime192v1")
  end if OpenSSL::OPENSSL_VERSION_NUMBER >= 0x10000000

  def test_no_compression
    ctx = Flipper.ssl_context(:ssl_no_compression => true)
    assert_equal Kgio::SSL::OP_NO_COMPRESSION,
                 ctx.options & Kgio::SSL::OP_NO_COMPRESSION

    ctx = Flipper.ssl_context({})
    assert_equal 0, ctx.options & Kgio::SSL::OP_NO_COMPRESSION
  end if OpenSSL::OPENSSL_VERSION_NUMBER >= 0x10000000
end
