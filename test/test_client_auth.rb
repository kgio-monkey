# -*- encoding: binary -*-
require "test/unit"
require "flipper"
Thread.abort_on_exception = true

class TestClientAuth < Test::Unit::TestCase

  def setup
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @port = @s_tcp.addr[1]
  end

  # used to ensure certs are setup correctly
  def test_pure_ruby_client_auth
    ctx = OpenSSL::SSL::SSLContext.new
    ctx.ca_file = "ca.crt"
    ctx.verify_depth = 1
    ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER |
                      OpenSSL::SSL::VERIFY_FAIL_IF_NO_PEER_CERT
    Kgio::Monkey!(ctx, :ssl_certificate_key => "server.key",
                       :ssl_certificate => "server.crt")
    @s_tcp.ssl_ctx = ctx
    good_client_check
    bad_client_check
  end

  def test_client_auth
    opts = {
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_client_certificate => "ca.crt",
      :ssl_verify_client => "on",
    }
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(opts)
    assert_equal 1, ctx.verify_depth
    good_client_check
    good_client_check("client2")
    bad_client_check
    assert_equal "FAILED", @last.ssl_client_verify
    assert_kind_of OpenSSL::SSL::SSLError, clueless_client_check
    assert_equal "NONE", @last.ssl_client_verify
    assert_kind_of OpenSSL::SSL::SSLError, optional_client_check
    assert_equal "NONE", @last.ssl_client_verify
  end

  def test_client_auth_optional
    opts = {
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_client_certificate => "ca.crt",
      :ssl_verify_client => "optional",
    }
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(opts)
    assert_equal 1, ctx.verify_depth
    good_client_check
    good_client_check("client2")
    bad_client_check
    assert_equal "HI", clueless_client_check
    assert_equal "NONE", @last.ssl_client_verify
    assert_equal "HI", optional_client_check
    assert_equal "NONE", @last.ssl_client_verify
  end

  def clueless_client_check
    thr = Thread.new { server_accept_fail }
    sock = TCPSocket.new(@host, @port)
    ctx = OpenSSL::SSL::SSLContext.new
    ssl = OpenSSL::SSL::SSLSocket.new(sock, ctx)
    ssl.hostname = @host
    ssl.sync_close = true
    ssl.connect
    ssl.write "HI"
    thr.value
    rescue => e
      e
  end

  def optional_client_check
    thr = Thread.new { server_accept_fail }
    sock = TCPSocket.new(@host, @port)
    ctx = OpenSSL::SSL::SSLContext.new
    ctx.ca_file = "ca.crt"
    ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    ssl = OpenSSL::SSL::SSLSocket.new(sock, ctx)
    ssl.hostname = @host
    ssl.sync_close = true
    ssl.connect
    ssl.write "HI"
    thr.value
    rescue => e
      e
  end

  def test_client_auth_sym
    opts = {
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_client_certificate => "ca.crt",
      :ssl_verify_mode => [ :PEER, :FAIL_IF_NO_PEER_CERT ]
    }
    @s_tcp.ssl_ctx = Flipper.ssl_context(opts)
    good_client_check
    good_client_check("client2")
    bad_client_check
  end

  def test_client_auth_str
    opts = {
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_client_certificate => "ca.crt",
      :ssl_verify_mode => %w(peer fail_if_no_peer_cert)
    }
    @s_tcp.ssl_ctx = Flipper.ssl_context(opts)
    good_client_check
    good_client_check("client2")
    bad_client_check
  end

  def good_client_check(prefix = 'client1')
    thr = Thread.new { server_accept_good }
    ctx = OpenSSL::SSL::SSLContext.new
    ctx.ca_file = "ca.crt"
    ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    ctx.key = OpenSSL::PKey::RSA.new(IO.read("#{prefix}.key"))
    ctx.cert = OpenSSL::X509::Certificate.new(IO.read("#{prefix}.crt"))
    sock = TCPSocket.new(@host, @port)
    ssl = OpenSSL::SSL::SSLSocket.new(sock, ctx)
    assert_nothing_raised do
      ssl.hostname = @host
      ssl.sync_close = true
      ssl.connect
      ssl.write "HI"
    end
    assert_equal "HI", thr.value
  end

  def bad_client_check
    thr = Thread.new { server_accept_fail }
    sock = TCPSocket.new(@host, @port)
    ctx = OpenSSL::SSL::SSLContext.new
    ctx.ca_file = "server.crt"
    ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    ssl = OpenSSL::SSL::SSLSocket.new(sock, ctx)
    ssl.hostname = @host
    ssl.sync_close = true
    assert_raises(OpenSSL::SSL::SSLError) do
      ssl.connect
      ssl.write("HI")
    end
    assert_kind_of OpenSSL::SSL::SSLError, thr.value

    thr = Thread.new { server_accept_fail }
    sock = TCPSocket.new(@host, @port)
    ctx = OpenSSL::SSL::SSLContext.new
    ctx.ca_file = "ca.crt"
    ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    ctx.key = OpenSSL::PKey::RSA.new(IO.read("bad-client.key"))
    ctx.cert = OpenSSL::X509::Certificate.new(IO.read("bad-client.crt"))
    ssl = OpenSSL::SSL::SSLSocket.new(sock, ctx)
    ssl.hostname = @host
    ssl.sync_close = true
    assert_raises(OpenSSL::SSL::SSLError) do
      ssl.connect
      ssl.write("HI")
    end
    assert_kind_of OpenSSL::SSL::SSLError, thr.value
  end

  def server_accept_fail
    @last = c = @s_tcp.kgio_accept
    c.kgio_read(2)
    rescue => e
      c.to_io.close
      e
  end

  def server_accept_good
    c = @s_tcp.kgio_accept
    rv = c.kgio_read(2)
    tmp = c.peer_cert
    assert_kind_of OpenSSL::X509::Certificate, tmp
    assert_kind_of String, c.ssl_client_serial
    assert_equal tmp.serial, c.ssl_client_serial.to_i(16)
    assert_kind_of String, c.ssl_cipher
    assert_kind_of String, c.ssl_client_i_dn
    assert_kind_of String, c.ssl_client_s_dn
    assert(c.ssl_client_s_dn != c.ssl_client_i_dn)
    s_dn = "/C=US/ST=Hell/L=A Very Special Place/O=Monkeys/" \
           "OU=Poo-Flingers/CN=127.0.0.1/emailAddress=kgio@bogomips.org"
    assert_equal s_dn, c.ssl_client_s_dn
    i_dn = "/C=US/ST=Hell/L=An Even More Special Place/O=Deranged Monkeys/" \
           "OU=Poo-Hurlers/CN=127.6.6.6/emailAddress=unicorn@bogomips.org"
    assert_equal i_dn, c.ssl_client_i_dn
    assert c.ssl_cipher.frozen?
    assert_kind_of String, c.ssl_session_id
    assert %w(TLSv1 TLSv1.2 SSLv2 SSLv3).include?(c.ssl_protocol)
    assert c.ssl_protocol.frozen?
    assert_match /-----BEGIN CERTIFICATE-----\n\t/s, c.ssl_client_cert
    assert_match /-----BEGIN CERTIFICATE-----\n\w/s, c.ssl_client_raw_cert
    assert_equal c.ssl_client_raw_cert.split(/\n/),
                 c.ssl_client_cert.split(/\n\t/)
    assert_equal "SUCCESS", c.ssl_client_verify
    assert c.ssl_client_verify.frozen?
    rv
  end
end if OpenSSL::SSL::SSLSocket.instance_methods.include?(:hostname=)
