# -*- encoding: binary -*-
require "test/unit"
require "kgio/monkey"
require "io/nonblock"
require "digest"
require "io/wait"
require "tempfile"
require "./test/common.rb"

class TestKgioFakeSendfile< Test::Unit::TestCase
  include MonkeyCommon

  def setup
    @to_close = []
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @s_ctx = OpenSSL::SSL::SSLContext.new
    @s_ctx.key = OpenSSL::PKey::RSA.new(IO.read("server.key"))
    @s_ctx.cert = OpenSSL::X509::Certificate.new(IO.read("server.crt"))
    assert_equal @s_ctx, Kgio::Monkey!(@s_ctx)
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @s_tcp.ssl_ctx = @s_ctx
    @port = @s_tcp.addr[1]
    @to_close << @s_tcp
  end

  def digester
    Thread.new do
      digest = Digest::SHA1.new
      io = TCPSocket.new(@host, @port)
      ssl = OpenSSL::SSL::SSLSocket.new(io)
      ssl.connect
      buf = ""
      while ssl.read(16384, buf)
        digest.update(buf)
      end
      ssl.close
      digest
    end
  end

  def counter(delay = nil)
    Thread.new do
      io = TCPSocket.new(@host, @port)
      ssl = OpenSSL::SSL::SSLSocket.new(io)
      ssl.connect
      buf = ""
      bytes = 0
      while ssl.read(16384, buf)
        sleep(delay) if delay
        bytes += buf.bytesize
      end
      ssl.close
      bytes
    end
  end

  def test_sendfile_full
    thr = digester
    bytes = 0
    accepted = @s_tcp.kgio_accept
    fp = File.open("random_blob")
    case rv = accepted.kgio_trysendfile(fp, bytes)
    when Symbol
      p [:rv, rv] if $DEBUG
      poll(accepted => rv)
    when Integer
      bytes == 0 && $DEBUG and p [ fp, fp.instance_variables ]
      bytes += rv
      p [ :wrote, bytes ] if $DEBUG
    when nil
      p [ :eof ] if $DEBUG
      break
    else
      assert false, "unexpected rv=#{rv.inspect}"
    end while true
    p [:closing ] if $DEBUG
    ensure_close(accepted)
    p [:done_closing ] if $DEBUG
    buf = ""
    assert_equal fp.stat.size, bytes
    assert_equal 0, fp.sysseek(0, IO::SEEK_CUR)
    sha1 = Digest::SHA1.new
    while fp.read(16384, buf)
      sha1.update(buf)
    end
    fp.close
    got = thr.value
    assert_equal sha1, got, "got=#{got} expect=#{sha1}"
    p [ @@poll_results, sha1 ] if $DEBUG
  end

  def test_sendfile_huge_sparse
    thr = digester
    bytes = 0
    accepted = @s_tcp.kgio_accept
    fp = Tempfile.new("sparse")
    fp.sysseek(0xffffffff)
    fp.syswrite "."
    case rv = accepted.kgio_trysendfile(fp, bytes)
    when Symbol
      p [:rv, rv] if $DEBUG
      poll(accepted => rv)
    when Integer
      bytes == 0 && $DEBUG and p [ fp, fp.instance_variables ]
      bytes += rv
      p [ :wrote, bytes ] if $DEBUG
    when nil
      p [ :eof ] if $DEBUG
      break
    else
      assert false, "unexpected rv=#{rv.inspect}"
    end while true
    p [:closing ] if $DEBUG
    ensure_close(accepted)
    p [:done_closing ] if $DEBUG
    assert_equal fp.stat.size, bytes
    fp.close
    got = thr.value
    expect = "f072875d4bd0aac0ac0ac6a714e2634787b5e69a"
    assert_equal expect, got.hexdigest, "got=#{got} expect=#{expect}"
    p [ @@poll_results, expect ] if $DEBUG
  end if ENV["SLOW_TESTS"]

  def test_sendfile_partial_offset_lt_page
    test_sendfile_partial_offset(4095)
  end

  def test_sendfile_partial_offset_gt_page
    test_sendfile_partial_offset(4097)
  end

  def test_sendfile_partial_offset(off = 4096)
    thr = digester
    accepted = @s_tcp.kgio_accept
    fp = File.open("random_blob", "rb")
    length = fp.stat.size - off
    bytes = 0
    assert length > 0, "#{fp.inspect} too small: #{fp.stat.size}"
    case rv = accepted.kgio_trysendfile(fp, length + bytes)
    when Symbol
      p [:rv, rv] if $DEBUG
      poll(accepted => rv)
    when Integer
      bytes == 0 && $DEBUG and p [ fp, fp.instance_variables ]
      bytes += rv
      p [ :wrote, bytes ] if $DEBUG
    when nil
      p [ :eof ] if $DEBUG
      break
    else
      assert false, "unexpected rv=#{rv.inspect}"
    end while true
    p [:closing ] if $DEBUG
    ensure_close(accepted)
    p [:done_closing ] if $DEBUG
    assert_equal off, bytes
    assert_equal 0, fp.sysseek(0, IO::SEEK_CUR)
    fp.sysseek(-off, IO::SEEK_END)
    sha1 = Digest::SHA1.new
    sha1.update(tmp = fp.sysread(off))
    fp.close
    assert_equal off, tmp.bytesize
    got = thr.value
    assert_equal sha1, got, "got=#{got} expect=#{sha1}"
    p [ @@poll_results, sha1 ] if $DEBUG
  end

  def test_sendfile_tip(off = 4096)
    thr = digester
    accepted = @s_tcp.kgio_accept
    fp = File.open("random_blob", "rb")
    bytes = 0
    limit = off
    case rv = accepted.kgio_trysendfile(fp, bytes, limit - bytes)
    when Symbol
      p [:rv, rv] if $DEBUG
      poll(accepted => rv)
    when Integer
      bytes == 0 && $DEBUG and p [ fp, fp.instance_variables ]
      bytes += rv
      p [ :wrote, bytes ] if $DEBUG
    when nil
      p [ :eof ] if $DEBUG
      break
    else
      assert false, "unexpected rv=#{rv.inspect}"
    end while true
    p [:closing ] if $DEBUG
    ensure_close(accepted)
    p [:done_closing ] if $DEBUG
    assert_equal off, bytes
    assert_equal 0, fp.sysseek(0, IO::SEEK_CUR)
    sha1 = Digest::SHA1.new
    sha1.update(fp.sysread(off))
    fp.close
    got = thr.value
    assert_equal sha1, got, "got=#{got} expect=#{sha1}"
    p [ @@poll_results, sha1 ] if $DEBUG
  end

  def test_sendfile_tip_lt_page
    test_sendfile_tip(4095)
  end

  def test_sendfile_tip_gt_page
    test_sendfile_tip(4097)
  end

  def test_sendfile_mmap_gc
    thr = counter
    accepted = @s_tcp.kgio_accept
    fp = File.open("random_blob", "rb")
    nr = 0
    bytes = 0
    case rv = accepted.kgio_trysendfile(fp.dup, 0, 4096)
    when Symbol
      p [:rv, rv] if $DEBUG
      poll(accepted => rv)
    when Integer
      bytes == 0 && $DEBUG and p [ fp, fp.instance_variables ]
      bytes += rv
      p [ :wrote, rv, bytes ] if $DEBUG
    when nil
      p [ :eof ] if $DEBUG
      break
    else
      assert false, "unexpected rv=#{rv.inspect}"
    end until (nr += 1) > 1000
    p [:closing ] if $DEBUG
    ensure_close(accepted)
    p [:done_closing ] if $DEBUG
    assert_equal 0, fp.sysseek(0, IO::SEEK_CUR)
    fp.close
    rv = thr.value
    assert_equal rv, bytes, "rv=#{rv} bytes=#{bytes}"
    p [ @@poll_results, bytes] if $DEBUG
  end
end
