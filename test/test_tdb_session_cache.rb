# -*- encoding: binary -*-
require "test/unit"
require "tempfile"
require "flipper"
require "./test/external_session_cache"
begin
  require "tdb"
rescue LoadError
end

class TestTDBSessionCache < Test::Unit::TestCase
  include TestExternalSessionCache
  NO_INTERNAL_STORE = OpenSSL::SSL::SSLContext::SESSION_CACHE_NO_INTERNAL_STORE
  def setup
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @port = @s_tcp.addr[1]
  end

  def test_session_cache_tdb_openssl
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => %w(tdb:666)
      )
    assert_equal NO_INTERNAL_STORE, ctx.session_cache_mode & NO_INTERNAL_STORE
    accepted = server_thread
    o, reused_count, tmp = run_openssl
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
  end

  def test_session_cache_multiple
    sess1 = Tempfile.new("sess.tdb")
    sess2 = Tempfile.new("sess.tdb")
    sess = Tempfile.new("sess")
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => [
          "tdb:#{sess1.path}:1",
          # still a small chance of failing with a large hash
          "tdb:#{sess2.path}:99999999999999999"
        ]
      )
    assert_equal NO_INTERNAL_STORE, ctx.session_cache_mode & NO_INTERNAL_STORE
    accepted = server_thread
    o, reused_count, tmp = run_openssl(['-sess_out', sess.path])
    assert_equal accepted.size, reused_count + 1

    o, tmp_count, tmp = run_openssl
    assert_equal reused_count, tmp_count

    o, reused_count_again, tmp = run_openssl(['-sess_in', sess.path])
    assert_equal reused_count + 1, reused_count_again
  end

  def test_session_cache_builtin_openssl
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => %w(builtin:666)
      )
    assert_equal 0, ctx.session_cache_mode & NO_INTERNAL_STORE
    accepted = server_thread
    o, reused_count, tmp = run_openssl
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
  end

  def test_session_cache_tdb_path_openssl
    a, b = IO.pipe
    c, d = IO.pipe
    sess = Tempfile.new("sessions")
    assert_equal 0, sess.size
    pid = fork do
      tdb = TDB.new(sess.path, :hash => :murmur2)
      b.syswrite('.')
      assert_equal '!', c.read(1)
      nr = 0
      tdb.each do |k,v|
        assert_kind_of OpenSSL::SSL::Session, OpenSSL::SSL::Session.new(v)
        nr += 1
      end
      exit(nr == 1)
    end
    assert_equal '.', a.read(1)
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => "tdb:#{sess.path}:666"
      )
    assert_equal NO_INTERNAL_STORE, ctx.session_cache_mode & NO_INTERNAL_STORE
    accepted = server_thread
    o, reused_count, tmp = run_openssl
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
    assert sess.size > 0
    d.syswrite('!')
    _, status = Process.waitpid2(pid)
    assert status.success?, status.inspect
    ensure
      [ a, b, c, d ].each { |io| io.close }
  end

  def test_session_cache_nil_openssl
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => nil
      )
    assert_equal 1, ctx.session_cache_size
    accepted = server_thread
    o, reused_count, tmp = run_openssl
    assert_equal 0, reused_count
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
  end

  def test_session_cache_false_openssl
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => false
      )
    assert_equal 0, ctx.session_cache_mode & NO_INTERNAL_STORE
    accepted = server_thread
    o, reused_count, tmp = run_openssl
    assert_equal 0, reused_count
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
  end

  def test_tdb_timeout_default_openssl
    sess = Tempfile.new('sess')
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => %w(tdb:666)
      )
    assert_equal NO_INTERNAL_STORE, ctx.session_cache_mode & NO_INTERNAL_STORE
    accepted = server_thread
    o, reused_count, tmp = run_openssl(['-sess_out', sess.path])
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
    session_id_a = o.grep(/Session-ID:/).uniq[0]
    sleep 2
    o, reused_count2, tmp = run_openssl(['-sess_in', sess.path])
    session_id_b = o.grep(/Session-ID:/).uniq[0]
    assert_equal session_id_a, session_id_b
    assert_equal reused_count + 1, reused_count2

    o, reused_count3, tmp = run_openssl(['-sess_in', sess.path])
    session_id_b = o.grep(/Session-ID:/).uniq[0]
    assert_equal session_id_a, session_id_b
    assert_equal reused_count + 1, reused_count3
  end

  def test_tdb_timeout_1s_openssl
    sess = Tempfile.new('sess')
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => %w(tdb:666),
      :ssl_session_timeout => 1
      )
    assert_equal NO_INTERNAL_STORE, ctx.session_cache_mode & NO_INTERNAL_STORE
    accepted = server_thread
    o, reused_count, tmp = run_openssl(['-sess_out', sess.path])
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
    session_id_a = o.grep(/Session-ID:/).uniq[0]
    sleep 2
    o, reused_count2, tmp = run_openssl(['-sess_in', sess.path])
    session_id_b = o.grep(/Session-ID:/).uniq[0]
    assert session_id_a != session_id_b
    assert_equal reused_count, reused_count2
  end

  def test_tdb_session_remove
    sess = Tempfile.new('sess')
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => %w(tdb:666)
      )
    accepted = server_thread
    o, reused_count, tmp = run_openssl(['-sess_out', sess.path])
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end
    session = OpenSSL::SSL::Session.new(sess.read)
    assert ctx.session_remove_cb.call(session)
    assert ! ctx.session_remove_cb.call(session)
  end

  def test_tdb_session_overflow
    sess = Tempfile.new('sess')
    @s_tcp.ssl_ctx = ctx = Flipper.ssl_context(
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_session_cache => %w(tdb:1)
      )
    assert_equal NO_INTERNAL_STORE, ctx.session_cache_mode & NO_INTERNAL_STORE
    accepted = server_thread
    o, reused_count, tmp = run_openssl(['-sess_out', sess.path])
    assert_equal accepted.size, reused_count + 1
    if o.grep(/Secure Renegotiation/).size > 0
      assert_equal accepted.size,
                   o.grep(/Secure Renegotiation IS supported/).size, tmp.read
    end

    o, tmp_count, tmp = run_openssl
    assert_equal reused_count, tmp_count

    o, reused_count_again, tmp = run_openssl(['-sess_in', sess.path])
    assert_equal reused_count, reused_count_again
  end
end if defined?(TDB) && defined?(TestExternalSessionCache)
