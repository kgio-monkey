# -*- encoding: binary -*-
require "test/unit"
require "kgio/monkey"
require "io/nonblock"
require "digest"
require "io/wait"
require "tempfile"
require "./test/common.rb"

class TestKgioMonkey < Test::Unit::TestCase
  include MonkeyCommon

  def setup
    @to_close = []
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @s_ctx = OpenSSL::SSL::SSLContext.new
    @s_ctx.key = OpenSSL::PKey::RSA.new(IO.read("server.key"))
    @s_ctx.cert = OpenSSL::X509::Certificate.new(IO.read("server.crt"))
    assert_equal @s_ctx, Kgio::Monkey!(@s_ctx)
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @s_tcp.ssl_ctx = @s_ctx
    @port = @s_tcp.addr[1]
    @to_close << @s_tcp
  end

  def teardown
    @to_close.each { |io| io.close unless io.closed? }
    @@poll_results.clear
  end

  def test_ssl_read_write
    sock = TCPSocket.new @host, @port
    @to_close << sock
    ssl = OpenSSL::SSL::SSLSocket.new(sock)
    accepted = Thread.new do
      client = @s_tcp.kgio_accept
      assert client.to_io.nonblock?, "not nonblocking?"
      case rc = client.kgio_tryread(1)
      when Symbol; poll(client => rc)
      when String
        assert_equal "H", rc
        break
      else
        assert false, "kgio_tryread failed rc=#{rc.inspect}"
      end while true

      if client.respond_to?(:nread)
        assert_equal 0, client.nread, "readahead should've drained"
      end
      assert_equal "I", client.kgio_tryread(1), "readahead should've buffered"
      client
    end
    connector = Thread.new do
      ssl.connect
      assert_equal 2, ssl.write("HI")
      true
    end
    assert connector
    client = accepted.value
    buf = "HELLO"
    assert_equal :wait_readable, client.kgio_tryread(5, buf)
    assert_equal "", buf
    expect = "0123456789"
    ssl.write(expect)
    poll(client => :wait_readable)
    tmp = client.kgio_tryread(10, buf)
    assert_equal expect, buf
    assert_equal tmp.object_id, buf.object_id
    assert_nil client.kgio_trywrite("")

    short_writes = []
    main_buf = buf = expect * 66666
    bytes = 0
    case rc = client.kgio_trywrite(buf)
    when Symbol
      fds = poll({client => rc}, 100)
      break if fds.nil?
    when String
      short_writes << rc.bytesize
      bytes += buf.bytesize - rc.bytesize
      buf = rc
    when nil
      bytes += buf.bytesize
      buf = main_buf
    else
      assert false, "rc=#{rc.inspect}"
    end while true

    warn("short writes empty: #{bytes} bytes written") if short_writes.empty?
    assert bytes > 0, "bytes=#{bytes}"
    warn "buffered bytes=#{bytes} #{ssl.to_io.nread rescue '-'}" if $DEBUG
    assert_equal "0123456789", ssl.read(10)

    assert_equal 5, ssl.write("HELLO")
    IO.select([client])
    assert_equal "", client.kgio_tryread(0)
    assert_equal "HELLO", client.kgio_tryread(5)
    thr = Thread.new { ssl.close }
    ensure_close(client)
    assert_nil thr.value
  end

  def test_curl_large_transfer
    r, w = IO.pipe
    @to_close << r << w
    curl_err = Tempfile.new("curl_err")
    nr = 100000
    buf = IO.read(__FILE__).freeze
    sha1 = Digest::SHA1.new
    nr.times { sha1.update(buf) }
    cmd = %w(curl -vsSf --cacert ca.crt)
    cmd << "https://#@host:#@port/"
    accepted = Thread.new { @s_tcp.kgio_accept }
    connector = fork do
      $stdout.reopen(w)
      w.close
      r.close
      $stderr.reopen(curl_err.path)
      exec(*cmd)
    end
    w.close
    accepted = accepted.value
    curl_sha1_thr = Thread.new do
      x = ""
      tmp_sha1 = Digest::SHA1.new
      while x = r.read(16384, x)
        tmp_sha1.update(x)
      end
      tmp_sha1
    end

    readed = []
    case rc = accepted.kgio_tryread(666)
    when Symbol
      break if rc == :wait_readable && readed[0]
      poll(accepted => rc)
    when String
      readed << rc
    else
      assert false, "kgio_tryread failed rc=#{rc.inspect}"
    end while true
    assert_match %r{\AGET / HTTP/1\.1\r\n}, readed.join

    nr.times do
      tmp = buf
      case rc = accepted.kgio_trywrite(tmp)
      when Symbol then poll(accepted => rc)
      when nil then break
      when String then tmp = rc
      else
        assert false, "kgio_trywrite didn't complete rc=#{rc.inspect}"
      end while true
    end
    ensure_close(accepted)
    _, status = Process.waitpid2(connector)
    assert status.success?, status.inspect
    if $DEBUG
      puts(curl_err.read)
      puts "poll_results: #{@@poll_results.inspect}"
    end
    expect = sha1.hexdigest
    got = curl_sha1_thr.value.hexdigest
    assert_equal expect, got, "expect=#{expect} got=#{got}"
  end if HAVE_CURL && ENV["SLOW_TESTS"]

  def test_curl_integration
    curl_out = Tempfile.new("curl_out")
    curl_err = Tempfile.new("curl_err")
    cmd = %w(curl -vsSf --cacert ca.crt)
    cmd << "https://#@host:#@port/"
    msg = "HTTP/1.1 200 OK\r\nConnection: close\r\nContent-Length: 8\r\n\r\n" \
          "Go Away\n"
    accepted = Thread.new { @s_tcp.kgio_accept }
    connector = fork do
      $stdout.reopen(curl_out.path)
      $stderr.reopen(curl_err.path)
      exec(*cmd)
    end
    accepted = accepted.value
    assert accepted.to_io.nonblock?, "not nonblocking?"

    readed = []

    case rc = accepted.kgio_tryread(666)
    when Symbol
      break if rc == :wait_readable && readed[0]
      poll(accepted => rc)
    when String
      readed << rc
    else
      assert false, "kgio_tryread failed rc=#{rc.inspect}"
    end while true
    assert_match %r{\AGET / HTTP/1\.1\r\n}, readed.join

    case rc = accepted.kgio_trywrite(msg)
    when Symbol
      poll(accepted => rc)
    when nil
      break
    else
      assert false, "kgio_trywrite didn't complete rc=#{rc.inspect}"
    end while true
    assert_equal @host, accepted.kgio_addr

    ensure_close(accepted)
    _, status = Process.waitpid2(connector)
    assert status.success?, status.inspect
    assert_equal "Go Away\n", curl_out.read
    if $DEBUG
      puts(curl_err.read)
      puts "poll_results: #{@@poll_results.inspect}"
    end
  end if HAVE_CURL

  def test_ssl_bad_client
    sock = TCPSocket.new @host, @port
    @to_close << sock
    OpenSSL::SSL::SSLSocket.new(sock)
    accepted = Thread.new do
      client = @s_tcp.kgio_accept
      begin
        assert client.to_io.nonblock?, "not nonblocking?"
        case rc = client.kgio_tryread(1)
        when Symbol; poll(client => rc)
        else
          assert false, "kgio_tryread failed rc=#{rc.inspect}"
        end while true
        true
      rescue => err
        err
      end
    end
    sock.close
    err = accepted.value
    assert err.backtrace.empty?, err.backtrace.inspect
  end

  def test_kgio_ssl_client_synchronous
    addr = Socket.pack_sockaddr_in @port, @host
    sock = Kgio::Socket.new addr
    @to_close << sock
    ctx = Kgio::Monkey!(OpenSSL::SSL::SSLContext.new)
    ssl = Kgio::SSLConnector.new(sock, ctx, @host)
    accepted = Thread.new do
      client = @s_tcp.kgio_accept
      case rc = client.kgio_read(1)
      when String
        assert_equal "H", rc
        break
      else
        assert false, "kgio_read failed rc=#{rc.inspect}"
      end while true
      client
    end
    assert_nil ssl.kgio_write("HI")
    client = accepted.value
    assert_equal "I", client.kgio_read(5)
    assert_equal :wait_readable, client.kgio_trypeek(1)
    assert_equal :wait_readable, client.kgio_tryread(5)

    assert_nil ssl.kgio_write("HELLO")
    client.to_io.kgio_wait_readable
    assert_equal "H", client.kgio_trypeek(1)
    buf = ""
    r = client.kgio_trypeek(5, buf)
    assert_equal "HELLO", r
    assert_equal r.object_id, buf.object_id
    assert_equal r.object_id, client.kgio_tryread(5, buf).object_id
    assert_equal "HELLO", r
    thr = Thread.new { client.kgio_peek(5) }
    assert_nil ssl.kgio_write("HELLO WORLD")
    assert_equal "HELLO", thr.value
    assert_equal "HELLO WORLD", client.kgio_read(11)
    thr = Thread.new { client.kgio_close }
    assert_nil ssl.kgio_close
    assert_nil thr.value
    assert ssl.closed?, "ssl not closed"
    assert ssl.to_io.closed?, "ssl io not closed"
    assert client.closed?, "client not closed"
    assert client.to_io.closed?, "client ssl not closed"
  end

  def test_client_post_connection_check
    ctx = OpenSSL::SSL::SSLContext.new
    ctx.ca_file = "ca.crt"
    ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    Kgio::Monkey!(ctx)

    io = Kgio::TCPSocket.new(@host, @port)
    thr = Thread.new { @s_tcp.kgio_accept.kgio_read(2) }
    ssl = Kgio::SSLConnector.new(io, ctx, @host)
    assert ! ssl.instance_variable_get(:@kgio_connected)
    assert_nil ssl.kgio_write("HI")
    assert_equal "HI", thr.value
    assert ssl.instance_variable_get(:@kgio_connected)

    io = Kgio::TCPSocket.new(@host, @port)
    thr = Thread.new { @s_tcp.kgio_accept.kgio_read(2) }
    ssl = Kgio::SSLConnector.new(io, ctx, "lolhost")
    assert ! ssl.instance_variable_get(:@kgio_connected)
    assert_raises(OpenSSL::SSL::SSLError) { ssl.kgio_write("HI") }
    assert ! ssl.instance_variable_get(:@kgio_connected)
    assert_nil ssl.kgio_tryclose
  end
end
