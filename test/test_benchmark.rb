# -*- encoding: binary -*-
require "test/unit"
require "tempfile"
require "benchmark"
require "flipper"
Thread.abort_on_exception = true
class TestBenchmark < Test::Unit::TestCase

  def setup
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @tmp = Tempfile.new("unix.sock")
    File.unlink(@tmp.path)
    @s_unix = Kgio::UNIXServer.new(@tmp.path)
    @s_unix.extend Kgio::SSLServer
  end

  def test_client_verify
    max = 5000
    nproc = 2
    opts = {
      :ssl_certificate => "server.crt",
      :ssl_certificate_key => "server.key",
      :ssl_client_certificate => "ca.crt",
      # we're benchmarking our performance, not OpenSSL here
      :ssl_ciphers => "LOW!kEDH",
      :ssl_verify_client => "on",
    }
    @s_unix.ssl_ctx = ctx = Flipper.ssl_context(opts)
    thr = Thread.new do
      (max * nproc).times do
        c = @s_unix.kgio_accept
        c.kgio_read(2)
        c.ssl_client_serial
        c.ssl_cipher
        c.ssl_client_i_dn
        c.ssl_client_s_dn
        c.ssl_cipher
        c.ssl_protocol
        c.ssl_client_cert
        c.ssl_client_verify
        c.kgio_close
      end
    end

    prefix = "client1"
    ctx = OpenSSL::SSL::SSLContext.new
    ctx.ca_file = "ca.crt"
    ctx.verify_mode = OpenSSL::SSL::VERIFY_PEER
    ctx.key = OpenSSL::PKey::RSA.new(IO.read("#{prefix}.key"))
    ctx.cert = OpenSSL::X509::Certificate.new(IO.read("#{prefix}.crt"))
    buf = "HI"
    path = @tmp.path

    clients = (1..nproc).map do
      fork do
        @s_unix.close
        max.times do
          sock = UNIXSocket.new(path)
          ssl = OpenSSL::SSL::SSLSocket.new(sock, ctx)
          ssl.hostname = @host
          ssl.sync_close = true
          ssl.connect
          ssl.write(buf)
          ssl.close
        end
      end
    end
    trash = (0..500000).map { |i| i.to_s }

    GC::Profiler.enable
    p(Benchmark.measure do
      thr.join
    end)
    GC::Profiler.report($stdout)
    clients.each do |c|
      p Process.waitpid2(c)
    end
  end
end if ENV["BENCHMARK"]
