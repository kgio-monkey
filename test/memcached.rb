require "./test/external_session_cache"

module TestMemcached
  include TestExternalSessionCache
  mc_path = `which memcached 2>/dev/null`.chomp
  MEMCACHED_PATH = File.executable?(mc_path) ? mc_path : nil

  def setup
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    mc = Kgio::TCPServer.new(@host, 0)
    @mc_port = mc.addr[1]
    mc.close
    @memcached_pid = fork do
      exec "#{MEMCACHED_PATH} -p#@mc_port -U#@mc_port -l#@host"
    end
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @port = @s_tcp.addr[1]
  end

  def teardown
    if @memcached_pid
      Process.kill(:TERM, @memcached_pid)
      Process.waitpid2(@memcached_pid)
      @memcached_pid = nil
    end
  end
end if defined?(TestExternalSessionCache)
