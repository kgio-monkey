require "test/unit"
require "flipper"
require "./test/common"

class Test::FlipperSessionCacheCurl < Test::Unit::TestCase
  include MonkeyCommon
  def setup
    @host = ENV["TEST_HOST"] || "127.0.0.1"
    @s_tcp = Kgio::TCPServer.new(@host, 0)
    @s_tcp.extend Kgio::SSLServer
    @port = @s_tcp.addr[1]
  end

  def test_curl_https
    ctx = Flipper.ssl_context(:ssl_certificate => "server.crt",
                              :ssl_certificate_key => "server.key")
    @s_tcp.ssl_ctx = ctx
    thr = Thread.new do
      begin
        2.times do
          c = @s_tcp.kgio_accept
          c.kgio_read(1666)
          c.kgio_write "HTTP/1.1 200 OK\r\nConnection: close\r\n\r\nbye\n"
          c.kgio_close
        end
        true
      rescue => e
        warn [:E, e.inspect]
        e
      end
    end
    url = "https://#@host:#@port/"
    cmd = "curl -sSf >/dev/null --cacert ca.crt --no-keepalive #{url} #{url}"
    assert system(cmd), cmd.inspect
    assert_equal true, thr.value
  end if HAVE_CURL
end
