Thread.abort_on_exception = true
require "kgio/ssl_server"

module MonkeyCommon
  @@poll_results = []
  HAVE_CURL = File.executable?(`which curl 2>/dev/null`.chomp)
  HAVE_OPENSSL_CLI = File.executable?(`which openssl`.chomp)

  def ensure_close(ssl)
    case rc = ssl.kgio_tryclose
    when Symbol
      poll({ssl => rc}, 100)
    when false
      assert ssl.to_io.closed?, "not closed (false)"
      break
    when nil
      assert ssl.to_io.closed?, "not closed (nil)"
      break
    else
      assert false, "kgio_tryclose didn't complete rc=#{rc.inspect}"
    end while true
    assert ssl.closed?, "not closed"
  end

  def poll(hash, timeout = nil)
    tmp = _poll(hash, timeout)
    @@poll_results << tmp
    tmp
  end

  def _poll(hash, timeout)
    Kgio.respond_to?(:poll) and return Kgio.poll(hash, timeout)
    rd, wr = [], []
    hash.each do |io,val|
      case val
      when :wait_readable
        rd << io
      when :wait_writable
        wr << io
      else
        assert false, "unknown val=#{val.inspect} for io=#{io.inspect}"
      end
    end
    IO.select(rd, wr, nil, timeout ? timeout / 1000.0 : nil)
  end
end
