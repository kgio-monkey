Thread.abort_on_exception = true
module TestExternalSessionCache
  def server_thread
    accepted = []
    Thread.new do
      loop do
        client = @s_tcp.kgio_accept
        accepted << client
        client.kgio_write("HI")
        client.kgio_close
      end
    end
    accepted
  end

  def run_openssl(args = nil)
    orig_err = $stderr.dup
    $stderr.reopen(tmp = Tempfile.new("tmperr"))
    cmd = "openssl s_client -ssl3 -connect #@host:#@port -CAfile ca.crt -reconnect"
    cmd << " #{args.join(' ')}" if args
    o = begin
      `#{cmd}`
    ensure
      $stderr.reopen(orig_err)
    end
    o = o.split(/\n/)
    reused_count = o.grep(/^Reused/).size
    tmp.rewind
    p [ :reused, reused_count, o.grep(/^Reused/) ] if $DEBUG
    [ o, reused_count, tmp ]
  end
end if OpenSSL::OPENSSL_VERSION_NUMBER >= 0x10000000 && RUBY_VERSION >= "1.9.3"
